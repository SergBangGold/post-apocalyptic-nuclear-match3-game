﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddDropToTeamA : MonoBehaviour
{
    private CombatSetupViewModleLink _link;

    private void Awake()
    {
        _link = GetComponentInParent<CombatSetupViewModleLink>();
    }

    public void Add(CharacterViewModel model)
    {
        _link.combatSetupViewModelImplementor.AddToASquad(model);
    }
}
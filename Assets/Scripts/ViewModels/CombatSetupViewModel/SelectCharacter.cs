﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityWeld.Binding;

public class SelectCharacter : MonoBehaviour, IPointerClickHandler
{
    private CombatSetupViewModleLink _link;
    private Template Template;

    private void Awake()
    {
        _link = GetComponentInParent<CombatSetupViewModleLink>();
        Template = GetComponent<Template>();
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            _link.combatSetupViewModelImplementor.SelectedVm = (CharacterViewModel) Template.GetViewModel();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityWeld.Binding;

public class RemoveFromATeam : MonoBehaviour , IPointerClickHandler
{
    private CombatSetupViewModleLink _link;
    private Template Template;

    private void Awake()
    {
        _link = GetComponentInParent<CombatSetupViewModleLink>();
        Template = GetComponent<Template>();
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
            _link.combatSetupViewModelImplementor.RemoveFromASquad((CharacterViewModel)Template.GetViewModel());
    }
}

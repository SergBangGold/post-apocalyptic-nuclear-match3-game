﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CombatSetupViewModleLink : MonoBehaviour
{
    [FormerlySerializedAs("CombatSetupViewModel")] [HideInInspector]
    public CombatSetupViewModelImplementor combatSetupViewModelImplementor; 
    private void Awake()
    {
        combatSetupViewModelImplementor = GetComponentInParent<CombatSetupViewModelImplementor>();
    } 
}

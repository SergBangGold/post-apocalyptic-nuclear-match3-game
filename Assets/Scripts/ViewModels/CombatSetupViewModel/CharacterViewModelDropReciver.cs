﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityWeld.Binding;

public class CharacterViewModelDropReciver : MonoBehaviour, IDropHandler
{
    private CombatSetupViewModleLink _link;

    public CharacterViewModelEvent ViewModelEvent;
    
    private void Awake()
    {
        _link = GetComponentInParent<CombatSetupViewModleLink>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            var provider = eventData.pointerDrag.GetComponent<IViewModelProvider>();

            CharacterViewModel character = provider.GetViewModel() as CharacterViewModel;
            if (character == null)
                Debug.LogError("not character view model");

            ViewModelEvent.Invoke(character);
        }
    }
    
    [Serializable]
    public class CharacterViewModelEvent : UnityEvent<CharacterViewModel>
    {
        
    }
}
﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    Vector3 starPosition;
    CanvasGroup _group;

    private void Awake()
    {
        _group = GetComponent<CanvasGroup>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        starPosition = transform.position;
        _group.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = transform.position + (Vector3) eventData.delta;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = starPosition;
        _group.blocksRaycasts = true;
    }
}
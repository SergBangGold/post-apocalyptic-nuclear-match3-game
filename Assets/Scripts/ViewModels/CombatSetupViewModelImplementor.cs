﻿using System.Collections.Generic;
using DefaultNamespace;
using DefaultNamespace.Components.CommonComponents;
using ECS.Implementors;
using UnityEngine;
using System;
using System.ComponentModel;
using DefaultNamespace.Engines;
using UnityWeld.Binding;


[Binding]
public class CombatSetupViewModelImplementor : MonoBehaviour, INotifyPropertyChanged, IImplementor, IEnablerComponent,
    IStarCombatComponent, ICombatSetupUIComponent
{
    public bool Enabled
    {
        get => gameObject.activeSelf;
        set => gameObject.SetActive(value);
    }

    public event Action OnClick;

    private List<float> _rankings;

    public void ClearAvaliable()
    {
        _characterViewModels.Clear();
    }

    public void AddAvaliable(List<CharacterDb.CharacterConfig> characters, List<float> characterDbRankings)
    {
        foreach (var charSetting in characters)
        {
            _characterViewModels.Add(new CharacterViewModel(charSetting, characterDbRankings));
        }

        _rankings = characterDbRankings;
        
        _teamA = new ObservableList<CharacterDb.CharacterConfig>();
        _teamA.CollectionChanged += TeamAOnCollectionChanged;
        _teamB = new ObservableList<CharacterDb.CharacterConfig>();
        _teamB.CollectionChanged += TeamBOnCollectionChanged;
    }


    private ObservableList<CharacterViewModel> _characterViewModels = new ObservableList<CharacterViewModel>();
    private ObservableList<CharacterViewModel> _teamAViewModels = new ObservableList<CharacterViewModel>();
    private ObservableList<CharacterViewModel> _teamBViewModels = new ObservableList<CharacterViewModel>();

    private CharacterViewModel _selectedVm =
        new CharacterViewModel(new CharacterDb.CharacterConfig(), new List<float>() {0, 0});


    [Binding] public ObservableList<CharacterViewModel> AvaliableCharacters => _characterViewModels;
    [Binding] public ObservableList<CharacterViewModel> TeamACharacters => _teamAViewModels;
    [Binding] public ObservableList<CharacterViewModel> TeamBCharacters => _teamBViewModels;

    [Binding]
    public CharacterViewModel SelectedVm
    {
        get { return _selectedVm; }
        set
        {
            _selectedVm = value;
            OnPropertyChanged("SelectedVm");
        }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void OnPropertyChanged(string selectedvm)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(selectedvm));
        }
    }


    [Binding]
    public void StartCombat()
    {
        OnClick?.Invoke();
    }

    public event Action<GameEngine.Teams, CharacterDb.CharacterConfig> AddToTeam;
    public event Action<GameEngine.Teams, CharacterDb.CharacterConfig> RemoveFromTeam;


    public ObservableList<CharacterDb.CharacterConfig> TeamA => _teamA;
    private ObservableList<CharacterDb.CharacterConfig> _teamA;


    public ObservableList<CharacterDb.CharacterConfig> TeamB => _teamB;
    private ObservableList<CharacterDb.CharacterConfig> _teamB;

    ////////////
    private void TeamAOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        TeamCollectionChange(GameEngine.Teams.A,e);
    }

    private void TeamBOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        TeamCollectionChange(GameEngine.Teams.B,e);
    }

    private void TeamCollectionChange(GameEngine.Teams team ,NotifyCollectionChangedEventArgs e)
    {
        var collection = team == GameEngine.Teams.A ? TeamACharacters : TeamBCharacters;

        switch (e.Action)
        {
            case NotifyCollectionChangedAction.Add:

                foreach (var item in e.NewItems)
                {
                    var typedItem = (CharacterDb.CharacterConfig)item;
                    collection.Add(new CharacterViewModel(typedItem, _rankings));
                }
                
                break;
            case NotifyCollectionChangedAction.Remove:
                
                foreach (var item in e.OldItems)
                {
                    var typedItem = (CharacterDb.CharacterConfig)item;

                    for (int i = 0; i < collection.Count; i++)
                    {
                        if (collection[i].Config == typedItem)
                        {
                            collection.Remove(collection[i]);
                            break;
                        }
                    }
                }
                
                break;
            case NotifyCollectionChangedAction.Reset:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
    }
    ////////////


    #region Model callbacks

    public void AddToASquad(CharacterViewModel characterViewModel)
    {
        AddToTeam?.Invoke(GameEngine.Teams.A,characterViewModel.Config);
    }

    public void AddToBSquad(CharacterViewModel characterViewModel)
    {
        AddToTeam?.Invoke(GameEngine.Teams.B,characterViewModel.Config);
    }

    public void RemoveFromASquad(CharacterViewModel characterViewModel)
    {
        RemoveFromTeam?.Invoke(GameEngine.Teams.A,characterViewModel.Config);
    }

    public void RemoveFromBSquad(CharacterViewModel characterViewModel)
    {
        RemoveFromTeam?.Invoke(GameEngine.Teams.B,characterViewModel.Config);
    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class CharacterViewModel : ICloneable
{
    [Binding] public float HP => _ranking[Config.rank] * (Config.hpRatio / (Config.hpRatio + 1));
    [Binding] public float Attack => _ranking[Config.rank] * (1 / (Config.hpRatio + 1));
    [Binding] public Sprite Sprite => Config.image;

    [Binding] public bool Orange => Config.type == 0;
    [Binding] public bool Red => Config.type == 1;
    [Binding] public bool Blue => Config.type == 2;
    [Binding] public bool Green=> Config.type == 3;


    public readonly CharacterDb.CharacterConfig Config;
    private List<float> _ranking;

    public CharacterViewModel(CharacterDb.CharacterConfig config, List<float> ranking)
    {
        Config = config;
        _ranking = ranking;
    }

    public object Clone()
    {
        return new CharacterViewModel(Config, _ranking);
    }
}
﻿using System;
using DefaultNamespace.Components.CommonComponents;
using ECS.Components.UI;
using ECS.Implementors;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class CombatViewModelImplementor : MonoBehaviour, IImplementor, IBackComponent , IEnablerComponent
{
    public event Action OnClick;

    public bool Enabled
    {
        get => gameObject.activeSelf;
        set => gameObject.SetActive(value);
    }

    [Binding]
    public void Back()
    {
        OnClick?.Invoke();
    }
}
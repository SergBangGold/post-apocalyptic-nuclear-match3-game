﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
#endif

namespace Utility
{
    public static class DataKeeper
    {
        private static string format = ".json";

        private static string FormFilePath(string fileName)
        {
//        #if UNITY_EDITOR
//        return  Directory.GetParent(Application.dataPath) + "/Builds/" + fileName + format;
//        #else
//            return Application.persistentDataPath + "/" + fileName + format;
//        #endif

            return Application.streamingAssetsPath + "/" + fileName + format;
        }

        public static void SaveToStreamingAssetsPath(object o, string fileName)
        {
//        string json = JsonConvert.SerializeObject(o, new JsonSerializerSettings(){TypeNameHandling = TypeNameHandling.All, Formatting =  Formatting.Indented});

#if UNITY_EDITOR
            string json = EditorJsonUtility.ToJson(o, true);
#else
        string json = JsonUtility.ToJson(o,true);
#endif


            try
            {
                using (var fs = new FileStream(FormFilePath(fileName), FileMode.Create, FileAccess.ReadWrite))
                {
                    using (var fw = new StreamWriter(fs))
                    {
                        fw.Write(json);
                        fw.Flush(); // Added
                    }
                }
            }
            catch (IOException ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public static T LoadFromStreamingAssetsPath<T>(string fileName) where T : new()
        {
            if (!File.Exists(FormFilePath(fileName)))
            {
                Debug.LogWarning("Loading data file not found for " + typeof(T));
                return default(T);
            }

            try
            {
                using (var fs = new FileStream(FormFilePath(fileName), FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    using (var sr = new StreamReader(fs))
                    {
                        string data = sr.ReadToEnd();

#if UNITY_EDITOR
                        T t = new T();
                        EditorJsonUtility.FromJsonOverwrite(data, t);
                        return t;
#else
                          return JsonUtility.FromJson<T>(data);
                    #endif
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }

            return default(T);
        }

        public static GameObject LoadGoFromResourse(string path)
        {
            return Resources.Load<GameObject>(path);
        }
    }
}
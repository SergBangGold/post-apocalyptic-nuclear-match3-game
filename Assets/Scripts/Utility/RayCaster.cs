using System;
using UnityEngine;

public interface IRayCaster
{
    bool CheckHit(Ray ray, float range, int layer, int mask, out Vector3 point, out int instanceID);
    bool CheckHit(Ray ray, float range, int mask, out Vector3 point);
    int GetHits(Vector3 position, float radius, int[] connectedArray);
}

public class RayCaster : IRayCaster
{
    private RaycastHit2D[] hits;

    public RayCaster()
    {
        hits = new RaycastHit2D[30];
    }

    public int GetHits(Vector3 position, float radius,  int[] connectedArray)
    {
        var count = Physics2D.CircleCastNonAlloc(position, radius, Vector2.zero, hits);

        for (int i = 0; i < count; i++)
        {
            connectedArray[i] = hits[i].transform.gameObject.GetInstanceID();
        }

        return count;
    }

    public bool CheckHit(Ray ray, float range, int layer, int mask, out Vector3 point, out int instanceID)
    {
        int hitsCount = Physics2D.RaycastNonAlloc(ray.origin, ray.direction, hits, range, mask);

        if (hitsCount == 0)
        {
            point = new Vector3();
            instanceID = -1;
            return false;
        }
        else
        {
            var hit = (hitsCount == 1) ? hits[0] : hits[1];

            point = hit.point;
//            if (hit.collider != null)
//            {
            var colliderGameObject = hit.collider.gameObject;

            if (colliderGameObject.layer == layer)
                instanceID = colliderGameObject.GetInstanceID();
            else
                instanceID = -1;

            return true;
//            }
        }
    }

    public bool CheckHit(Ray ray, float range, int mask, out Vector3 point)
    {
        throw new NotImplementedException();
//        var hit = Physics2D.Raycast(ray.origin, ray.direction, range, mask);
//        if (hit == true)
//        {
//            point = hit.point;
//            if (hit.collider != null)
//            {
//                return true;
//            }
//        }
//
//        point = new Vector3();
//        return false;
    }
}
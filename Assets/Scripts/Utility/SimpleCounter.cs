using UnityEditorInternal.Profiling.Memory.Experimental;

namespace Utility
{
    public interface ICounter
    {
        int NextIndex { get; }
    }

    public  class SimpleCounter : ICounter
    {
        private static int _count;

        static SimpleCounter()
        {
            _count = 0;
        }

        public  int NextIndex
        {
            get
            {
                _count++;
                return _count;
            }
        }
    }
}
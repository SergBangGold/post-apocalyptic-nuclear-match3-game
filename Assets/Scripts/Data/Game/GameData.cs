using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Data.Game
{
    [Serializable]
    public class GameData
    {
        public const string FileName = "GameData";
        public List<GameObject> tokenPrefabs = new List<GameObject>();
        public List<GameObject> tokenBonusPrefabs = new List<GameObject>();
        public GameObject linePrefab;

        [FormerlySerializedAs("UIComabtDamage")] [Header("UI")] public GameObject UICombat;
            [Header("UI")] public GameObject UICombatSetup;
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[Serializable]
public class CharacterDb
{
    public const string FileName = "CharacterDb";

    public List<CharacterConfig> CharacterSettings = new List<CharacterConfig>();
    [FormerlySerializedAs("RankValue")] public List<float> Rankings = new List<float>();

    [Serializable]
    public class CharacterConfig
    {
        public Sprite image;
        public int rank = 1;
        public int type = 0;
        public float hpRatio = 4;
    }
}
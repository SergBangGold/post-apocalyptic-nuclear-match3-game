using UnityEngine;
using Utility;

namespace Data.Game
{
    public class GameDataViewer : MonoBehaviour
    {
        public GameData GameData;
        public GameSettings GameSettings;
        public CharacterDb CharacterDb;

        [ContextMenu("Save")]
        public void Save()
        {
            DataKeeper.SaveToStreamingAssetsPath(GameData, GameData.FileName);
            DataKeeper.SaveToStreamingAssetsPath(GameSettings, GameSettings.FileName);
            DataKeeper.SaveToStreamingAssetsPath(CharacterDb, CharacterDb.FileName);
        }

        [ContextMenu("Load")]
        public void Load()
        {
            GameData = DataKeeper.LoadFromStreamingAssetsPath<GameData>(GameData.FileName);
            GameSettings = DataKeeper.LoadFromStreamingAssetsPath<GameSettings>(GameSettings.FileName);
            CharacterDb = DataKeeper.LoadFromStreamingAssetsPath<CharacterDb>(CharacterDb.FileName);
        }
    }
}
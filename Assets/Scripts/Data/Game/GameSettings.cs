using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Data.Game
{
    [Serializable]
    public class GameSettings
    {
        public const string FileName = "GameSettings";
        
        [FormerlySerializedAs("TokenSize")] public  float ConnectionDistance = 1f;
        public  float DistanceBetweenCameraAndTokens = 20f;
        
        [Header("BonusExplosion")] 
        public float ExplosionRadius = 1f;

    }
}
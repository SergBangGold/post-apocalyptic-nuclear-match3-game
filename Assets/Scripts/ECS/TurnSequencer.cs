using DefaultNamespace.Engines.Input;
using DefaultNamespace.Engines.Turn;
using ECS.Descriptors.Token;
using ECS.Engines.Token;
using Svelto.ECS;
using UnityEngine.Networking;

namespace DefaultNamespace
{
    public class TurnSequencer : Sequencer<TurnSequencer>
    {
        public void SetSequence(
            TokenTypeStoreEngine tokenTypeStoreEngine,
            TokenTypeRemoveEngine tokenTypeRemoveEngine,
            //
            HoverTokenStoreEngine hoverTokenStoreEngine,
            HoverTokenRemoveEngine hoverTokenRemoveEngine,
            //
//            TurnResultCalculationEngine turnResultCalculationEngine,
            BonusTokenSpawnEngine bonusTokenSpawnEngine,
            //
            TurnTokensActivateUnselectDesigion tokensActivateUnselectDesigion,
            TokenUnSelectEngine tokenUnSelectEngine,
            TokenSplitActivateEngine tokenSplitActivateEngine,
            //
            TokenDisppearEngine disppearEngine,
            //
            TokenSelectEngine tokenSelectEngine,
            //
            TokenEnablePhysicsEngine tokenEnablePhysicsEngine,
            TokenDisablePhysicsEngine tokenDisablePhysicsEngine,
            //
            TokenDespawnEngine tokenDespawnEngine,
            //
            TurnBeginHandlerEngine turnBeginHandlerEngine,
            TurnInProgressHandlerEngine turnInProgressHandlerEngine,
            TurnEndingHandlerEngine turnEndingHandlerEngine,
            TurnResetHandlerEngine turnResetHandlerEngine,
            TurnActivationDoneTrackerEngine turnActivationDoneTrackerEngine
        )
        {
            base.SetSequence(
                new Steps(
                    new Step()
                    {
                        from = turnBeginHandlerEngine,
                        to = new To(tokenTypeStoreEngine,
                            hoverTokenStoreEngine
                        )
                    },
                    new Step()
                    {
                        from = turnInProgressHandlerEngine,
                        to = new To(
                            tokenSelectEngine,
//                            turnResultCalculationEngine,
                            hoverTokenStoreEngine
                        )
                    },
                    new Step()
                    {
                        from = turnEndingHandlerEngine,
                        to = new To(
                            tokensActivateUnselectDesigion
//                            tokenSplitActivateEngine,
//                            tokenUnSelectEngine
                            )
                    },
                    new Step()
                    {
                        from = tokenSplitActivateEngine,
                        to = new To(tokenDisablePhysicsEngine
                            //token activate engine
                            //token BONUS activate engine
                            //disppearEngine
                            )
                    },
                    new Step()
                    {
                        from = turnActivationDoneTrackerEngine,
                        to = new To(
                            bonusTokenSpawnEngine,
                            tokenEnablePhysicsEngine,
                            tokenTypeRemoveEngine,
                            hoverTokenRemoveEngine,
                          //  tokenDespawnEngine,
                            turnResetHandlerEngine
                        )
                    },
                    new Step()
                    {
                        from = tokenUnSelectEngine,
                        to = new To(
                            tokenTypeRemoveEngine,
                            hoverTokenRemoveEngine,
                            turnResetHandlerEngine
                        )
                    }
                )
            );
        }
    }
}
using ECS.Components.UI;
using UnityEngine;
using UnityEngine.UI;

namespace ECS.Implementors.UI
{
    public class TextComponentImplementor : MonoBehaviour , IImplementor, ITextComponent
    {
        public Text _Text;
        
        public void UpdateText(string text)
        {
            _Text.text = text;
        }
    }
}
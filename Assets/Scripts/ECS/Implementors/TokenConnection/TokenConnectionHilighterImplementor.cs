using System.Collections.Generic;
using DefaultNamespace.Components.CommonComponents;
using ECS.Components.TokenConnection;
using ECS.Implementors;
using UnityEngine;

namespace DefaultNamespace
{
    public class TokenConnectionHilighterImplementor : UnityEngine.MonoBehaviour, ITokenConnectionHilighter,
        IEnablerComponent,
        IImplementor
    {
        public LineRenderer LineRenderer;

        public bool Enabled
        {
            set => gameObject.SetActive(value);
        }

        public void UpdatePoints(List<Vector3> list)
        {
            var generatedPoints = Generate_Points(list.ToArray(),10);
        
            LineRenderer.positionCount = generatedPoints.Length;
            LineRenderer.SetPositions(generatedPoints);
        }
        
        /*
         * public bool Enabled
                  {
                      set => gameObject.SetActive(value);
                  }
          
                  public void UpdatePoints(List<Vector3> list)
                  {
                      LineRenderer.positionCount = list.Count;
                      LineRenderer.SetPositions(list.ToArray());
                  }
         * 
         */
        
        Vector3[] Generate_Points(Vector3[] keyPoints, int segments=100){
            Vector3[] Points = new Vector3[(keyPoints.Length - 1) * segments + keyPoints.Length];
            for(int i = 1; i < keyPoints.Length;i++){
                Points [(i - 1) * segments + i - 1] = new Vector3(keyPoints [i-1].x,keyPoints [i-1].y,0);
                for (int j = 1;j<=segments;j++){
                    float x = keyPoints [i - 1].x;
                    float y = keyPoints [i - 1].y;
                    float z = 0;//keyPoints [i - 1].z;
                    float dx = (keyPoints [i].x - keyPoints [i - 1].x)/segments;
                    float dy = (keyPoints [i].y - keyPoints [i - 1].y)/segments;
                    Points [(i - 1) * segments + j + i - 1] = new Vector3 (x+dx*j,y+dy*j,z);
                }
            }
            Points [(keyPoints.Length - 1) * segments + keyPoints.Length - 1] = new Vector3(keyPoints [keyPoints.Length-1].x,keyPoints [keyPoints.Length-1].y,0);
            return Points;
        }
    }
}
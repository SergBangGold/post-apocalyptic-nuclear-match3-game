using DefaultNamespace;
using ECS.Components.Token;
using Spine;
using Spine.Unity;
using Svelto.ECS;
using UnityEngine;

namespace ECS.Implementors.Token
{
    public class TokenHilightSpineImplementor : MonoBehaviour, IImplementor, IHilightComponent
    {
        public SkeletonAnimation SkeletonAnimation;

        [SpineAnimation()] public string selectedAnimation;
        [SpineAnimation()] public string normalAnimation;
        [SpineAnimation()] public string hoveredAnimation;
        [SpineAnimation()] public string disapperAnimation;

        public bool _selected;
        public bool _hovered;
        public bool _disapper;

        public bool Disappear
        {
            set
            {
                _disapper = value;

                
                UpdateAnimation();

                gameObject.transform.parent.gameObject.layer =
                    (value) ? GAME_LAYERS.IGNORE_LAYER : GAME_LAYERS.TOKEN_LAYER;
                
            }
        }


        public bool Selected
        {
            set
            {
                _selected = value;
                UpdateAnimation();
            }
        }

        private void UpdateAnimation()
        {
            if (_disapper)
            {
                SkeletonAnimation.AnimationState.SetAnimation(0, disapperAnimation, false);
            }
            else if (_hovered)
            {
                SkeletonAnimation.AnimationState.SetAnimation(0, hoveredAnimation, true);
            }
            else if (_selected)
            {
                SkeletonAnimation.AnimationState.SetAnimation(0, selectedAnimation, true);
            }
            else
            {
                SkeletonAnimation.AnimationState.SetAnimation(0, normalAnimation, true);
            }
        }


        public bool Hovered
        {
            set
            {
                _hovered = value;

                //Dirty here , heirarchial animation states might do better
                if (_hovered == false && _disapper == true)
                    return;

                UpdateAnimation();
            }
        }
    }
}
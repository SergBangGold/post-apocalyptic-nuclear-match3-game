using DefaultNamespace.Components.CommonComponents;
using ECS.Implementors;
using UnityEngine;

namespace DefaultNamespace.Implementors.Token
{
    public class TokenImplementor : MonoBehaviour, IImplementor, IPositionCompoenent, IEnablerComponent
    {
        private Transform _transform;
        private GameObject _gameObject;

        public bool Enabled
        {
            set => _gameObject.SetActive(value);
        }

        private void Awake()
        {
            _gameObject = gameObject;
            _transform = transform;
        }

        public Vector3 position
        {
            get => _transform.position;
            set => _transform.position = value;
        }
    }
}
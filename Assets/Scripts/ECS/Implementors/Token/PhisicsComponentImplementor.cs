using ECS.Components.Token;
using UnityEngine;

namespace ECS.Implementors.Token
{
    public class PhisicsComponentImplementor : MonoBehaviour , IPhisicsComponent , IImplementor
    {
        public Rigidbody2D _Rigidbody2D;

        public bool Enabled
        {
            set
            {
                _Rigidbody2D.bodyType = value ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic;

                if (!value)
                {
                    _Rigidbody2D.velocity = Vector2.zero;
                }
            }
        }
    }
}
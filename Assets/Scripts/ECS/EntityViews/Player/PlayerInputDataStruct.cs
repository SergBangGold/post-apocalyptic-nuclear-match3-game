using Svelto.ECS;
using UnityEngine;

namespace ECS.EntityViews.Player
{
    public struct PlayerInputDataStruct : IEntityStruct
    {
        public Ray MouseRay;
        public bool MouseDown;
        public bool IsMouseDown;
        public bool MouseUp;

        public EGID ID { get; set; }
    }
}
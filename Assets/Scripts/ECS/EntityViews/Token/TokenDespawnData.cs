using Svelto.ECS;

namespace ECS.EntityViews.Token
{
    public struct TokenDespawnData : IEntityStruct
    {
        public EGID storeEGID;
        public EGID ID { get; set; }
    }
}
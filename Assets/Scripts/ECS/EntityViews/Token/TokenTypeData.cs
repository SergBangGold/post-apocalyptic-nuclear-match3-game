using Svelto.ECS;

namespace ECS.EntityViews.Token
{
    public struct TokenTypeData : IEntityStruct
    {
        public int type;
        public EGID ID { get; set; }
    }
}
using DefaultNamespace.Components.CommonComponents;
using ECS.Components.Token;
using Svelto.ECS;

namespace DefaultNamespace.EntityViews.Token
{
    public struct TokenEntityViewStruct : IEntityViewStruct
    {
        public IPositionCompoenent PositionCompoenent;
        public IEnablerComponent EnablerComponent;
        public IHilightComponent HilightComponent;
        public IPhisicsComponent PhysicsComponent; 

        public EGID ID { get; set; }
    }
}
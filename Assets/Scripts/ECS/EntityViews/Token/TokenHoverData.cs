using Svelto.ECS;
using UnityEngine;

namespace ECS.EntityViews.Token
{
    public struct TokenHoverData : IEntityStruct
    {    
        public EGID storeEGID;
        public EGID ID { get; set; }
        public Vector3 LastHoverPosition { get; set; }
    }
}
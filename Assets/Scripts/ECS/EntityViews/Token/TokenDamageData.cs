using Svelto.ECS;

namespace ECS.EntityViews.Token
{
    public struct TokenDamageData : IEntityStruct
    {
        public float Value;
        public EGID ID { get; set; }
    }
}
using DefaultNamespace.Components.CommonComponents;
using Svelto.ECS;

namespace DefaultNamespace.EntityViews.UI.Combat
{
    public struct CombatUISetupEntityView : IEntityViewStruct
    {
        public IStarCombatComponent StarCombatComponent;
        public IEnablerComponent EnablerComponent;
        public ICombatSetupUIComponent CombatSetupUiComponent;
        
        public EGID ID { get; set; }
    }
}
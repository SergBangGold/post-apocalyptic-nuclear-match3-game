using DefaultNamespace.Components.CommonComponents;
using ECS.Components.UI;
using Svelto.ECS;

namespace DefaultNamespace.EntityViews.UI.Combat
{
    public struct CombatUIEntityView : IEntityViewStruct
    {
        public IBackComponent BackClicked;
        public IEnablerComponent EnablerComponent;
        public EGID ID { get; set; }
    }
}
using ECS.Components.UI;
using Svelto.ECS;

namespace DefaultNamespace.EntityViews.UI.Combat
{
    public struct CombatDamageEntityView : IEntityViewStruct
    {
        public ITextComponent _textComponent;
        public EGID ID { get; set; }
    }
}
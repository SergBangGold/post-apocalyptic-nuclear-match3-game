using Svelto.ECS;
using UnityEngine;

namespace DefaultNamespace.EntityViews.TokenConnection
{
    public struct TokenConnectionPointData : IEntityStruct
    {
        public Vector3 position;
        public EGID ID { get; set; }
    }
}
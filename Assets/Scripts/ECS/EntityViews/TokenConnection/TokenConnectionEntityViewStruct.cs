using DefaultNamespace.Components.CommonComponents;
using ECS.Components.TokenConnection;
using Svelto.ECS;

namespace ECS.EntityViews.TokenConnection
{
    public struct TokenConnectionEntityViewStruct : IEntityViewStruct
    {
        public ITokenConnectionHilighter ConnectionHilighter;
        public IEnablerComponent EnablerComponent;
        public EGID ID { get; set; }
    }
}
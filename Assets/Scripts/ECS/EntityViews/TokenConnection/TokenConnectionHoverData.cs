using Svelto.ECS;

namespace ECS.EntityViews.TokenConnection
{
    public struct TokenConnectionHoverData : IEntityStruct
    {
        public EGID storeEGID;
        public EGID ID { get; set; }
    }
}
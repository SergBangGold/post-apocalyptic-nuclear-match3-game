using Svelto.ECS;

namespace DefaultNamespace.EntityViews.Turn
{
    public struct Turn: IEntityStruct
    {
        public EGID ID { get; set; }
    }
}
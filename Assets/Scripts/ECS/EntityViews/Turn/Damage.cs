using Svelto.ECS;

namespace DefaultNamespace.EntityViews.Turn
{
    public struct Damage : IEntityStruct
    {
        public int Type;
        public float Ammount;
    
        public EGID ID { get; set; }
    }
}
using Svelto.ECS;

namespace DefaultNamespace.EntityViews.Turn
{
    public struct TurnResultData : IEntityStruct
    {
        public int Damage;
        public int TokenCount;
        public EGID ID { get; set; }
    }
}
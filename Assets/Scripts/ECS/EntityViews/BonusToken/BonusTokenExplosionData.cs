using Svelto.ECS;

namespace DefaultNamespace.EntityViews.BonusToken
{
    public struct BonusTokenExplosionData: IEntityStruct
    {
        public EGID ID { get; set; }
    }
}
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using DefaultNamespace.EntityViews.TokenConnection;
using ECS.Descriptors.Token;
using ECS.Descriptors.TokenConnection;
using ECS.EntityViews.Token;
using ECS.EntityViews.TokenConnection;
using Svelto.ECS;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Experimental.PlayerLoop;
using Utility;

namespace ECS.Engines.TokenConnection
{
    public class TokenConnectionEngine : IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }

        private IEntityFactory _entityFactory;
        private ICounter _counter;
        private IEntityFunctions _entityFunctions;

        private List<Vector3> list; 

        public TokenConnectionEngine(IEntityFactory entityFactory, ICounter counter, IEntityFunctions entityFunctions)
        {
            _entityFactory = entityFactory;
            _counter = counter;
            _entityFunctions = entityFunctions;

            list = new List<Vector3>();
        }

        public void Ready()
        {
            UpdateConnections().Run();
        }

        IEnumerator UpdateConnections()
        {
            while (true)
            {
                var hovers = entitiesDB.QueryEntities<TokenHoverData>(ECSGroups.Player, out var hoverCount);
                var storeHovers =
                    entitiesDB.QueryEntities<TokenConnectionHoverData>(ECSGroups.ConnectionLine, out var storeHoverCount);

                //no input, no handling
                if (hoverCount == 0 && storeHoverCount == 0)
                {
                    yield return null;
                    continue;
                }

                //input started , start handling
                if (hoverCount != 0 && storeHoverCount == 0)
                {
                    var storeEgid = new EGID(_counter.NextIndex, ECSGroups.ConnectionLine);
                    var initilizer = _entityFactory.BuildEntity<TokenConnectionHoverDescriptor>(storeEgid, null);
                    initilizer.Init(new TokenConnectionHoverData() {storeEGID = hovers[0].storeEGID});

                    CreateConnectionPointEntity(ref hovers[0].storeEGID);
                }

                //input complete , cleanup
                if (hoverCount == 0 && storeHoverCount != 0)
                {
                    //remove store data
                    var storeEgids = entitiesDB.QueryEntities<TokenConnectionHoverData>(ECSGroups.ConnectionLine, out _);
                    _entityFunctions.RemoveEntity<TokenConnectionHoverDescriptor>(storeEgids[0].ID);

                    //remove points
                    var linePoints =
                        entitiesDB.QueryEntities<TokenConnectionPointData>(ECSGroups.ConnectionLine,
                            out var pointCount);
                    for (int i = 0; i < pointCount; i++)
                    {
                        _entityFunctions.RemoveEntity<TokenConnectionPointDescriptor>(linePoints[i].ID);
                    }

                    HideLine();
                }

                //input in progress
                if (hoverCount != 0 && storeHoverCount != 0)
                {
                    //if same hover that the last frame, skip it 
                    if (hovers[0].storeEGID == storeHovers[0].storeEGID)
                    {
                        yield return null;
                        continue;
                    }

                    //else we update storage 
                    storeHovers[0].storeEGID = hovers[0].storeEGID;

                    //create point
                    CreateConnectionPointEntity(ref storeHovers[0].storeEGID);
                    yield return  null;
                    UpdateView();
                    continue;

                }

                yield return null;
            }
        }

        private void UpdateView()
        {
            var points =
                entitiesDB.QueryEntities<TokenConnectionPointData>(ECSGroups.ConnectionLine, out var pointCount);
            if (pointCount < 2)
                return;
            
            var line = entitiesDB.QueryEntities<TokenConnectionEntityViewStruct>(ECSGroups.ConnectionLine,out var lineCount);
            if (lineCount != 1)
                return;

            ShowLine();
            
            list.Clear();
            for (int i = 0; i < pointCount; i++)
            {
                list.Add(points[i].position);
            }

            line[0].ConnectionHilighter.UpdatePoints(list);
            
        }

        private void CreateConnectionPointEntity(ref EGID egid)
        {
            //create point
            var tokenViews =
                entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(egid,
                    out uint index);
            var position = tokenViews[index].PositionCompoenent.position;

            var positionEgid = new EGID(_counter.NextIndex, ECSGroups.ConnectionLine);
            var initilizer = _entityFactory.BuildEntity<TokenConnectionPointDescriptor>(positionEgid, null);
            initilizer.Init(new TokenConnectionPointData() {position = position});
        }

        private void ShowLine()
        {
            var lines = entitiesDB.QueryEntities<TokenConnectionEntityViewStruct>(ECSGroups.ConnectionLine,
                out _);

            lines[0].EnablerComponent.Enabled = true;
        }

        private void HideLine()
        {
            var lines = entitiesDB.QueryEntities<TokenConnectionEntityViewStruct>(ECSGroups.ConnectionLine,
                out _);

            lines[0].EnablerComponent.Enabled = false;
        }
    }
}
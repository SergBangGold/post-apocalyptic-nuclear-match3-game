using System.Collections;
using DefaultNamespace;
using DefaultNamespace.EntityViews.Turn;
using DefaultNamespace.EntityViews.UI.Combat;
using Svelto.ECS;
using Svelto.Tasks;

namespace ECS.Engines.UI
{
    public class UiDamageUpdaterEngine : SingleEntityEngine<TurnResultData>, IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }

        private ITaskRoutine _taskRoutine;

        public void Ready()
        {
            _taskRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumeratorProvider(UpdateCombatDamage);
        }

        protected override void Add(ref TurnResultData entityView)
        {
            _taskRoutine.Start();
        }

        protected override void Remove(ref TurnResultData entityView)
        {
            _taskRoutine.Stop();
            UpdateText("0");
        }

        private IEnumerator UpdateCombatDamage()
        {
            while (true)
            {
                if (entitiesDB.HasAny<CombatDamageEntityView>(ECSGroups.CombatUI) == false)
                {
                    yield return null;
                    continue;
                }

                var turnReslut = entitiesDB.QueryEntities<TurnResultData>(ECSGroups.TurnInProgress, out _);
                UpdateText(turnReslut[0].Damage.ToString());
                
                yield return null;
            }
        }

        private void UpdateText(string text)
        {
            var combatDamageView = entitiesDB.QueryEntities<CombatDamageEntityView>(ECSGroups.CombatUI, out var count);
            if (count == 0)
            {
                return;
            }

            combatDamageView[0]._textComponent.UpdateText(text);
        }
    }
}
using System;
using System.Collections;
using DefaultNamespace;
using DefaultNamespace.EntityViews.BonusToken;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.EntityViews.Player;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;

namespace ECS.Engines.Token
{
    public class TokenUnSelectEngine : IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }

        private IEntityFunctions _entityFunctions;
        private TurnSequencer _turnSequencer;


        public TokenUnSelectEngine(IEntityFunctions entityFunctions, TurnSequencer turnSequencer)
        {
            _entityFunctions = entityFunctions;
            _turnSequencer = turnSequencer;
        }

        public void Ready()
        {
            Deactivate().Run();
        }

        private IEnumerator Deactivate()
        {
            while (true)
            {
                if (entitiesDB.HasAny<TokenEntityViewStruct>(ECSGroups.PrepareToDeActivate))
                {
                    var selected =
                        entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.PrepareToDeActivate, out var count);

                    _turnSequencer.Next(this, new EGID());

                    for (int i = 0; i < count; i++)
                    {
                        selected[i].HilightComponent.Selected = false;
                        selected[i].HilightComponent.Hovered = false;
                        _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selected[i].ID, ECSGroups.VisableTokens);
                    }
                }

                yield return null;
            }
        }
    }
}
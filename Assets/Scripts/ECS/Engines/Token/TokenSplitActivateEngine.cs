using System.Collections;
using DefaultNamespace;
using DefaultNamespace.EntityViews.BonusToken;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.EntityViews.Player;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using Svelto.Tasks.Enumerators;
using Svelto.Tasks.Unity;
using UnityEngine;
using Utility;

namespace ECS.Engines.Token
{
    public class TokenSplitActivateEngine : IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }
        private TurnSequencer _turnSequencer;
        private IEntityFunctions _entityFunctions;

        public TokenSplitActivateEngine(TurnSequencer turnSequencer, IEntityFunctions entityFunctions)
        {
            _entityFunctions = entityFunctions;
            _turnSequencer = turnSequencer;
        }

        public void Ready()
        {
            Split().Run();
        }

        IEnumerator Split()
        {
            while (true)
            {
                var selected =
                    entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.PrepareToActivate,
                        out var totalSelectedCount);

                if (totalSelectedCount > 0)
                {
                    var selectedDamage =
                        entitiesDB.QueryEntities<TokenDamageData>(ECSGroups.PrepareToActivate, out var damageCount);
                    for (int i = 0; i < damageCount; i++)
                    {
                        _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selectedDamage[i].ID,
                            ECSGroups.ReadyToActivateTokens);
                    }

                    var selectedBoom =
                        entitiesDB.QueryEntities<BonusTokenExplosionData>(ECSGroups.PrepareToActivate,
                            out var explpsionCount);
                    for (int i = 0; i < explpsionCount; i++)
                    {
                        _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selectedBoom[i].ID,
                            ECSGroups.ReadyToActivateExplosionTokens);
                    }

                    _turnSequencer.Next(this, new EGID());
                }

                yield return null;
            }
        }
    }
}
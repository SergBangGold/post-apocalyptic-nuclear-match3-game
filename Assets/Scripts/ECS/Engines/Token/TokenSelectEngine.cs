using System;
using System.Collections;
using Data.Game;
using DefaultNamespace;
using DefaultNamespace.EntityViews.BonusToken;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.EntityViews.Player;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

namespace ECS.Engines.Token
{
    public class TokenSelectEngine : IStep, IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }

        private IEntityFunctions _entityFunctions;
        private IRayCaster _rayCaster;

        private  GameSettings _settings;
        
        public TokenSelectEngine(IEntityFunctions entityFunctions, IRayCaster rayCaster , GameSettings gameSettings)
        {
            _settings = gameSettings;
            _entityFunctions = entityFunctions;
            _rayCaster = rayCaster;
        }

        public void Ready()
        {
        }


        public void Step(EGID __)
        {
            var inputs = entitiesDB.QueryEntities<PlayerInputDataStruct>(ECSGroups.Player, out _);
            if (inputs[0].IsMouseDown == false)
            {
                return;
            }

            //if we hit an object
            Vector3 hit;
            int id;
            if (_rayCaster.CheckHit(inputs[0].MouseRay, _settings.DistanceBetweenCameraAndTokens,
                GAME_LAYERS.TOKEN_LAYER, GAME_LAYERS.TOKEN_MASK, out hit,
                out id))
            {
                // if object is in active tokens
                var hoverEGID = new EGID(id, ECSGroups.VisableTokens);
                if (entitiesDB.Exists<TokenTypeData>(hoverEGID))
                {
                    //we check if there is a select type
                    var selections = entitiesDB.QueryEntities<TokenTypeData>(ECSGroups.Player, out var selectionsCount);
                    if (selectionsCount == 0)
                    {
                        return;
                    }

                    //first token we add it
                    if (entitiesDB.HasAny<TokenTypeData>(ECSGroups.SelectedTokens) == false)
                    {
                        MoveTokenToSelected(hoverEGID);
                        return;
                    }

                    //not the first token do checks
                    //if object colors dont match we skip it
                    var tokenData = entitiesDB.QueryEntitiesAndIndex<TokenTypeData>(hoverEGID, out var index);
                    if (tokenData[index].type != selections[0].type)
                    {
                        return;
                    }

                    //we check if there is a select type
                    var lastHovers =
                        entitiesDB.QueryEntities<TokenHoverData>(ECSGroups.Player, out var lastSelectCount);
                    if (lastSelectCount == 0)
                    {
                        return;
                    }

                    //if not raycasted from the last one we skip it
                    //last hover token
                    var lastSelected = entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(
                        lastHovers[0].storeEGID, out uint lasthoverIndex);

                    var currentTokens = entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(
                        hoverEGID, out var currentHoverIndex);

                    var ray = new Ray(lastSelected[lasthoverIndex].PositionCompoenent.position,
                        currentTokens[currentHoverIndex].PositionCompoenent.position -
                        lastSelected[lasthoverIndex].PositionCompoenent.position);

#if UNITY_EDITOR
                    Debug.DrawRay(ray.origin, ray.direction, Color.yellow);
#endif

                    if (_rayCaster.CheckHit(ray, _settings.ConnectionDistance, GAME_LAYERS.TOKEN_LAYER, GAME_LAYERS.TOKEN_MASK, out hit,
                        out var hitid))
                    {
                        //if we hit the same token
                        if (hitid == id)
                        {
                            MoveTokenToSelected(hoverEGID);
                        }
                    }
                }
            }
        }

        private void MoveTokenToSelected(EGID gid)
        {
            //else we move it to selected
            entitiesDB.ExecuteOnEntity(gid, (ref TokenEntityViewStruct view) =>
                view.HilightComponent.Selected = true);


            _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(gid, ECSGroups.SelectedTokens);
        }
    }
}
using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using Svelto.ECS;

namespace ECS.Engines.Token
{
    public class TokenEnablePhysicsEngine : IQueryingEntitiesEngine, IStep
    {
        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
        }

        public void Step( EGID id)
        {
            foreach (ExclusiveGroup exclusiveGroup in ECSGroups.DisableGravityTokens)
            {
                var tokens = entitiesDB.QueryEntities<TokenEntityViewStruct>(exclusiveGroup, out var count);
                for (int i = 0; i < count; i++)
                {
                    tokens[i].PhysicsComponent.Enabled = true;
                }
            }
        }
    }
}
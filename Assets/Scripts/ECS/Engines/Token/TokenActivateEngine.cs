using System.Collections;
using DefaultNamespace;
using DefaultNamespace.Descriptors.Turn;
using DefaultNamespace.EntityViews.Token;
using DefaultNamespace.EntityViews.Turn;
using ECS.Descriptors.Token;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using Utility;

namespace ECS.Engines.Token
{
    public class TokenActivateEngine : IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }
        ITaskRoutine _taskRoutine;
        private IEntityFunctions _entityFunctions;
        private IEntityFactory _entityFactory;
        private ICounter _counter;

        public TokenActivateEngine(IEntityFunctions entityFunctions, IEntityFactory entityFactory, ICounter counter)
        {
            _counter = counter;
            _entityFactory = entityFactory;
            _entityFunctions = entityFunctions;
            _taskRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(Run());
        }

        public void Ready()
        {
            _taskRoutine.Start();
        }

        IEnumerator Run()
        {
            while (true)
            {
                var tokens =
                    entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.ReadyToActivateTokens, out var count);
                var damages = entitiesDB.QueryEntities<TokenDamageData>(ECSGroups.ReadyToActivateTokens, out _);
                var types = entitiesDB.QueryEntities<TokenTypeData>(ECSGroups.ReadyToActivateTokens, out _);

                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var egid = new EGID(_counter.NextIndex, ECSGroups.Player);
                        var initilizer = _entityFactory.BuildEntity<DamageDescriptor>(egid, null);
                        initilizer.Init(new Damage() {Type = types[i].type, Ammount = damages[i].Value});

                        _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(tokens[i].ID, ECSGroups.ActivatedTokens);
                    }
                }


                yield return null;
            }
        }
    }
}
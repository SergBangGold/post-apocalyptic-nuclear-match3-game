using System.Collections;
using Data.Game;
using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.Factories.Token;
using Svelto.ECS;
using UnityEngine;

namespace ECS.Engines.Token
{
    public class TokenSpawnerEngine : IQueryingEntitiesEngine , IStep
    {
        public IEntitiesDB entitiesDB { get; set; }

        private ITokenFactory _tokenFactory;
        private IEntityFunctions _entityFunctions;
        
        public TokenSpawnerEngine(ITokenFactory tokenFactory, IEntityFunctions entityFunctions)
        {
            _tokenFactory = tokenFactory;
            _entityFunctions = entityFunctions;
        }

        public void Ready()
        {
            CheckForSpawn().Run();
        }

        IEnumerator CheckForSpawn()
        {
            for (int i = 0; i < 40; i++)
            {
                SpawnToken();
                yield return null;
            }

            while (true)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    SpawnToken();
                }

               

                yield return null;
            }
        }

        private void SpawnToken()
        {
            var id = Random.Range(0, 4);
            var fromGroupId = (int) ECSGroups.CachedTokens +
                              id;

            if (entitiesDB.HasAny<TokenEntityViewStruct>(fromGroupId))
            {
                ReuseEnemy(fromGroupId);
            }
            else
                _tokenFactory.Build( id,Vector3.zero);
        }

        private void ReuseEnemy(int fromGroupId)
        {
            int count;
            var tokens = entitiesDB.QueryEntities<TokenEntityViewStruct>(fromGroupId, out count);

            if (count > 0)
            {
                tokens[0].PositionCompoenent.position = Vector3.zero;
                tokens[0].EnablerComponent.Enabled = true;

                tokens[0].HilightComponent.Selected = false;
                tokens[0].HilightComponent.Hovered = false;
                tokens[0].HilightComponent.Disappear = false;

                tokens[0].PhysicsComponent.Enabled = true;
                
                _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(tokens[0].ID,
                    ECSGroups.VisableTokens);
            }
        }

        public void Step(EGID id)
        {
            var activeTokens =
                entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.VisableTokens, out var count);
            while (count < 40)
            {
                count ++;
                SpawnToken();
            }
        }

    }
}
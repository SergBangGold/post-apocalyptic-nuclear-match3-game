using DefaultNamespace.EntityViews.Token;
using ECS.EntityViews.Token;
using Svelto.ECS;

namespace ECS.Engines.Token
{
    public class TokenHoverOverAnimationEngine : SingleEntityEngine<TokenHoverData>, IQueryingEntitiesEngine
    {
        protected override void Add(ref TokenHoverData entityView)
        {
            var entitiy = entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(entityView.storeEGID, out var index);
            entitiy[index].HilightComponent.Hovered = true;
        }

        protected override void Remove(ref TokenHoverData entityView)
        {
            if (entitiesDB.Exists<TokenEntityViewStruct>(entityView.storeEGID))
            {
                var entitiy =
                    entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(entityView.storeEGID, out var index);
                entitiy[index].HilightComponent.Hovered = false;
            }
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
        }
    }
}
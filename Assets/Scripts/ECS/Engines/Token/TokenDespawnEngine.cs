using System.Collections;
using DefaultNamespace.EntityViews.BonusToken;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.EntityViews.Player;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using Svelto.Tasks.Enumerators;
using Svelto.Tasks.Unity;
using UnityEngine;

namespace DefaultNamespace
{
    public class TokenDespawnEngine : IQueryingEntitiesEngine //, IStep
    {
        private IEntityFunctions _entityFunctions;
        public IEntitiesDB entitiesDB { get; set; }

        ITaskRoutine _routine;

        public void Ready()
        {
            Despawn().Run();
        }

        public TokenDespawnEngine(IEntityFunctions entityFunctions)
        {
            _entityFunctions = entityFunctions;
            _routine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumeratorProvider(Despawn)
                .SetScheduler(new UpdateMonoRunner(""));
        }

//        public void Step(EGID id)
//        {
//            _routine.Start();
//        }

        IEnumerator Despawn()
        {
            while (true)
            {
                var selectedDamage =
                    entitiesDB.QueryEntities<TokenDamageData>(ECSGroups.DisapearedTokens, out var damageCount);
                for (int i = 0; i < damageCount; i++)
                {
                    _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selectedDamage[i].ID,
                        ECSGroups.ReadyToCacheTokens);
                }

                var selectedBoom =
                    entitiesDB.QueryEntities<BonusTokenExplosionData>(ECSGroups.DisapearedTokens,
                        out var explpsionCount);
                for (int i = 0; i < explpsionCount; i++)
                {
                    _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selectedBoom[i].ID,
                        ECSGroups.ReadyToCacheTokens + 1);
                }

                yield return null;

                var basicDespawns =
                    entitiesDB.QueryEntities<TokenTypeData>(ECSGroups.ReadyToCacheTokens, out var despawnCount);
                var basicViews =
                    entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.ReadyToCacheTokens, out var _);

                for (int i = 0; i < despawnCount; i++)
                {
                    DespawnToken(ref basicViews[i]);
                    _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(basicDespawns[i].ID,
                        ECSGroups.CachedTokens + basicDespawns[i].type);
                }

                var bonusDespawns =
                    entitiesDB.QueryEntities<TokenTypeData>(ECSGroups.ReadyToCacheTokens + 1, out var bonusCount);
                var bonusViews =
                    entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.ReadyToCacheTokens + 1, out var _);

                for (int i = 0; i < bonusCount; i++)
                {
                    DespawnToken(ref bonusViews[i]);
                    _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(bonusDespawns[i].ID,
                        ECSGroups.CachedBonusTokens + bonusDespawns[i].type);
                }
            }
        }


        private void DespawnToken(ref TokenEntityViewStruct selectedView)
        {
            selectedView.EnablerComponent.Enabled = false;
            selectedView.HilightComponent.Selected = false;
        }
    }
}
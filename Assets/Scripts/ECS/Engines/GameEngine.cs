using System;
using System.Collections;
using System.Diagnostics;
using Data.Game;
using DefaultNamespace.EntityViews.UI.Combat;
using Svelto.ECS;
using UnityEngine;

namespace DefaultNamespace.Engines
{
    public class GameEngine : MultiEntitiesEngine<CombatUIEntityView, CombatUISetupEntityView>, IQueryingEntitiesEngine
    {
        public enum Teams{A, B}
        
        public IEntitiesDB entitiesDB { get; set; }

        private CharacterDb _characterDb;

        public GameEngine(CharacterDb characterDb)
        {
            _characterDb = characterDb;
        }


        protected override void Add(ref CombatUISetupEntityView entityView)
        {
            entityView.StarCombatComponent.OnClick += StarCombatComponentOnOnClick;
            entityView.CombatSetupUiComponent.ClearAvaliable();
            entityView.CombatSetupUiComponent.AddAvaliable(_characterDb.CharacterSettings, _characterDb.Rankings);
            
            entityView.CombatSetupUiComponent.AddToTeam+=CombatSetupUiComponentOnAddToTeam;
            entityView.CombatSetupUiComponent.RemoveFromTeam+=CombatSetupUiComponentOnRemoveFromTeam;
        }


        private void StarCombatComponentOnOnClick()
        {
            StateCombat();
        }

        protected override void Remove(ref CombatUISetupEntityView entityView)
        {
            entityView.StarCombatComponent.OnClick -= StarCombatComponentOnOnClick;
        }
        
        

        ////////squad////////
        
        private void CombatSetupUiComponentOnRemoveFromTeam(Teams arg1, CharacterDb.CharacterConfig arg2)
        {
            var combatUiSetupEntityViews =
                entitiesDB.QueryEntities<CombatUISetupEntityView>(ECSGroups.CombatSetupUI, out var setupCount);

            switch (arg1)
            {
                case Teams.A:
                    combatUiSetupEntityViews[0].CombatSetupUiComponent.TeamA.Remove( arg2);
                    break;
                case Teams.B:
                    combatUiSetupEntityViews[0].CombatSetupUiComponent.TeamB.Remove( arg2);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(arg1), arg1, null);
            }
            
        }

        private void CombatSetupUiComponentOnAddToTeam(Teams arg1, CharacterDb.CharacterConfig arg2)
        {
            var combatUiSetupEntityViews =
                entitiesDB.QueryEntities<CombatUISetupEntityView>(ECSGroups.CombatSetupUI, out var setupCount);

            switch (arg1)
            {
                case Teams.A:
                    combatUiSetupEntityViews[0].CombatSetupUiComponent.TeamA.Add( arg2);
                    break;
                case Teams.B:
                    combatUiSetupEntityViews[0].CombatSetupUiComponent.TeamB.Add( arg2);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(arg1), arg1, null);
            }
        }
        
        ////////////////

        protected override void Add(ref CombatUIEntityView entityView)
        {
            entityView.BackClicked.OnClick += CombatUIBack;
        }

        private void CombatUIBack()
        {
            StateSetupCombat();
        }

        protected override void Remove(ref CombatUIEntityView entityView)
        {
            entityView.BackClicked.OnClick -= CombatUIBack;
        }

        ////////////////

        private void StateSetupCombat()
        {
            var combatUiSetupEntityViews =
                entitiesDB.QueryEntities<CombatUISetupEntityView>(ECSGroups.CombatSetupUI, out var setupCount);
            if (setupCount != 0)
                combatUiSetupEntityViews[0].EnablerComponent.Enabled = true;

            var combatUi = entitiesDB.QueryEntities<CombatUIEntityView>(ECSGroups.CombatUI, out var combatUis);
            if (combatUis != 0)
                combatUi[0].EnablerComponent.Enabled = false;
        }

        private void StateCombat()
        {
            var combatUiSetupEntityViews =
                entitiesDB.QueryEntities<CombatUISetupEntityView>(ECSGroups.CombatSetupUI, out var setupCount);
            if (setupCount != 0)
                combatUiSetupEntityViews[0].EnablerComponent.Enabled = false;

            var combatUi = entitiesDB.QueryEntities<CombatUIEntityView>(ECSGroups.CombatUI, out var combatUis);
            if (combatUis != 0)
                combatUi[0].EnablerComponent.Enabled = true;
        }
        ////////////////

        public void Ready()
        {
            SetAtStart().Run();
        }

        IEnumerator SetAtStart()
        {
            while (true)
            {
                var combatUiSetupEntityViews =
                    entitiesDB.QueryEntities<CombatUISetupEntityView>(ECSGroups.CombatSetupUI, out var setupCount);

                var combatUi = entitiesDB.QueryEntities<CombatUIEntityView>(ECSGroups.CombatUI, out var combatUis);

                if (setupCount != 0 && combatUis != 0)
                    break;

                yield return null;
            }

            StateSetupCombat();
        }
    }
}
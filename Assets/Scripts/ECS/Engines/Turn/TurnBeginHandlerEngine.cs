using System.Collections;
using System.Net.NetworkInformation;
using DefaultNamespace.Descriptors.Turn;
using DefaultNamespace.EntityViews.Turn;
using ECS.EntityViews.Player;
using Svelto.ECS;
using Svelto.Tasks;
using Utility;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnBeginHandlerEngine : IQueryingEntitiesEngine
    {
        private TurnSequencer _turnSequencer;
        private IEntityFunctions _entityFunctions;
        private ITaskRoutine _routine;

        private IEntityFactory _entityFactory;
        private ICounter _counter;

        public TurnBeginHandlerEngine(TurnSequencer turnSequencer, IEntityFunctions entityFunctions, ICounter counter,
            IEntityFactory entityFactory)
        {
            _turnSequencer = turnSequencer;
            _entityFunctions = entityFunctions;

            _routine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(Run());

            _entityFactory = entityFactory;
            _counter = counter;
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
            _routine.Start();
        }

        IEnumerator Run()
        {
            while (true)
            {
                if (entitiesDB.HasAny<EntityViews.Turn.Turn>(ECSGroups.TurnBegin) == true)
                {
                    var input = entitiesDB.QueryEntities<PlayerInputDataStruct>(ECSGroups.Player, out var inputsCount);

                    if (input[0].MouseDown)
                    {
                        var turns = entitiesDB.QueryEntities<EntityViews.Turn.Turn>(ECSGroups.TurnBegin, out var _);

                        _entityFunctions.SwapEntityGroup<TurnDescriptor>(turns[0].ID, ECSGroups.TurnInProgress);
                        _turnSequencer.Next(this, new EGID());


                        var egid = new EGID(_counter.NextIndex, ECSGroups.TurnInProgress);
                        _entityFactory.BuildEntity<TurnResultDescriptor>(egid, null);

                        var damage = entitiesDB.QueryEntities<Damage>(ECSGroups.Player, out var damageCount);
                        for (int i = 0; i < damageCount; i++)
                        {
                            _entityFunctions.RemoveEntity<DamageDescriptor>(damage[i].ID);
                        }
                    }
                }

                yield return null;
            }
        }
    }
}
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using Svelto.ECS;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnTokensActivateUnselectDesigion : IQueryingEntitiesEngine, IStep
    {
        public IEntitiesDB entitiesDB { get; set; }

        private IEntityFunctions _entityFunctions;

        public TurnTokensActivateUnselectDesigion(IEntityFunctions entityFunctions)
        {
            _entityFunctions = entityFunctions;
        }

        public void Ready()
        {
        }

        public void Step(EGID _)
        {
            var selected =
                entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.SelectedTokens, out var totalSelectedCount);

            bool enoughToActivate = totalSelectedCount >= 3;
            //more then 3 => activate

            for (int i = 0; i < totalSelectedCount; i++)
                _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selected[i].ID,
                    enoughToActivate ? ECSGroups.PrepareToActivate : ECSGroups.PrepareToDeActivate);
        }
    }
}
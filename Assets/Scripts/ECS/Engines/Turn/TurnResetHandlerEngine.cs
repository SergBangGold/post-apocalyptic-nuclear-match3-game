using DefaultNamespace.Descriptors.Turn;
using DefaultNamespace.EntityViews.Turn;
using Svelto.ECS;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnResetHandlerEngine : IQueryingEntitiesEngine, IStep
    {
        public IEntitiesDB entitiesDB { get; set; }

        private IEntityFunctions _entityFunctions;

        public TurnResetHandlerEngine(IEntityFunctions entityFunctions)
        {
            _entityFunctions = entityFunctions;
        }

        public void Ready()
        {
        }

        public void Step(EGID id)
        {
            var turns = entitiesDB.QueryEntities<EntityViews.Turn.Turn>(ECSGroups.TurnComplete, out var _);
            _entityFunctions.SwapEntityGroup<TurnDescriptor>(turns[0].ID, ECSGroups.TurnBegin);

            var turnResuls =
                entitiesDB.QueryEntities<TurnResultData>(ECSGroups.TurnInProgress, out var _);
            _entityFunctions.RemoveEntity<TurnResultDescriptor>(turnResuls[0].ID);
        }
    }
}
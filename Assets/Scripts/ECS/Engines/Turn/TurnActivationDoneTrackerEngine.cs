using System.Collections;
using DefaultNamespace.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnActivationDoneTrackerEngine : SingleEntityEngine<EntityViews.Turn.Turn>, IQueryingEntitiesEngine
    {
        private ITaskRoutine _routine;
        private TurnSequencer _turnSequencer;
        private IEntityFunctions _entityFunctions;

        public TurnActivationDoneTrackerEngine(TurnSequencer turnSequencer, IEntityFunctions entityFunctions)
        {
            _turnSequencer = turnSequencer;
            _entityFunctions = entityFunctions;
            _routine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumeratorProvider(TrackTurnComplete);
        }

        protected override void Add(ref EntityViews.Turn.Turn entityView)
        {
            if (entityView.ID.groupID == (int) ECSGroups.TurnComplete)
                _routine.Start();
        }

        protected override void Remove(ref EntityViews.Turn.Turn entityView)
        {
            if (entityView.ID.groupID == (int) ECSGroups.TurnComplete)
                _routine.Stop();
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
        }

        IEnumerator TrackTurnComplete()
        {
            while (true)
            {
                bool hasAny = false;
                foreach (ExclusiveGroup exclusiveGroup in ECSGroups.TurnInProgressMarkerGroup)
                {
                    if (entitiesDB.HasAny<TokenEntityViewStruct>(exclusiveGroup))
                    {
                        hasAny = true;
                        break;
                    }
                }

                if (hasAny)
                {
                    yield return null;
                    continue;
                }

                _turnSequencer.Next(this, new EGID());
                yield break;
            }
        }
    }
}
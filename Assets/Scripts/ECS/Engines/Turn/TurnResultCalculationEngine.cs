using System;
using System.Collections;
using DefaultNamespace.Descriptors.Turn;
using DefaultNamespace.EntityViews.Turn;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Utility;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnResultCalculationEngine : IQueryingEntitiesEngine //, IStep
    {
        public IEntitiesDB entitiesDB { get; set; }

        public TurnResultCalculationEngine()
        {
        }

        public void Ready()
        {
            Step().Run();
        }

        public IEnumerator Step()
        {
            while (true)
            {
                var turnResuls =
                    entitiesDB.QueryEntities<TurnResultData>(ECSGroups.TurnInProgress, out var turnCount);

                if (turnCount != 0)
                {
                    var damages = entitiesDB.QueryEntities<Damage>(ECSGroups.Player, out var damageCount);

                    float ammount = 0;
                    for (int i = 0; i < damageCount; i++)
                    {
                        ammount += damages[i].Ammount;
                    }

                    //update
                    turnResuls[0].Damage = (int) ammount;
                    turnResuls[0].TokenCount = (int) ammount;
                }

                yield return null;
            }
        }
    }
}
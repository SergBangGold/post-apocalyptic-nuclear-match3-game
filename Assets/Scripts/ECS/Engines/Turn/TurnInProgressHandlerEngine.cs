using System.Collections;
using DefaultNamespace.Descriptors.Turn;
using ECS.EntityViews.Player;
using Svelto.ECS;
using Svelto.Tasks;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnInProgressHandlerEngine : IQueryingEntitiesEngine
    {
        private TurnSequencer _turnSequencer;
        private IEntityFunctions _entityFunctions;
        private ITaskRoutine _routine;

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {_routine.Start();
        }

        public TurnInProgressHandlerEngine(TurnSequencer turnSequencer, IEntityFunctions entityFunctions)
        {
            _turnSequencer = turnSequencer;
            _entityFunctions = entityFunctions;
            _routine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(Run());
        }

        IEnumerator Run()
        {
            while (true)
            {
                if (entitiesDB.HasAny<EntityViews.Turn.Turn>(ECSGroups.TurnInProgress) == true)
                {
                    var input = entitiesDB.QueryEntities<PlayerInputDataStruct>(ECSGroups.Player, out var inputsCount);

                    if (input[0].MouseUp)
                    {
                        var turns = entitiesDB.QueryEntities<EntityViews.Turn.Turn>(ECSGroups.TurnInProgress, out var _);

                        _entityFunctions.SwapEntityGroup<TurnDescriptor>(turns[0].ID,ECSGroups.TurnEnding);

                        yield return null;
                        continue;
                    }
                    
                    _turnSequencer.Next(this,new EGID());
                }
                
                yield return null;
            }
        }
    }
}
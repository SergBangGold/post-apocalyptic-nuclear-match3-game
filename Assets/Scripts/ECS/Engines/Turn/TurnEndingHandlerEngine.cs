using System.Collections;
using DefaultNamespace.Descriptors.Turn;
using Svelto.ECS;
using Svelto.Tasks;

namespace DefaultNamespace.Engines.Turn
{
    public class TurnEndingHandlerEngine : IQueryingEntitiesEngine
    {
        private TurnSequencer _turnSequencer;
        private IEntityFunctions _entityFunctions;
       

        public IEntitiesDB entitiesDB { get; set; }

        public TurnEndingHandlerEngine(TurnSequencer turnSequencer, IEntityFunctions entityFunctions)
        {
            _turnSequencer = turnSequencer;
            _entityFunctions = entityFunctions;
       
        }

        public void Ready()
        {
            Run().Run();
        }

        IEnumerator Run()
        {
            while (true)
            {
                if (entitiesDB.HasAny<EntityViews.Turn.Turn>(ECSGroups.TurnEnding) == true)
                {
                   
                    var turns = entitiesDB.QueryEntities<EntityViews.Turn.Turn>(ECSGroups.TurnEnding, out var _);
                    _entityFunctions.SwapEntityGroup<TurnDescriptor>(turns[0].ID, ECSGroups.TurnComplete);
                    
                    _turnSequencer.Next(this, new EGID());
                }

                yield return null;
            }
        }
    }
}
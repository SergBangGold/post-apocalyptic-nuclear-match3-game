using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using DefaultNamespace.EntityViews.Turn;
using ECS.Descriptors.Token;
using ECS.EntityViews.Token;
using ECS.Factories.Token;
using Svelto.ECS;
using UnityEngine;

namespace ECS.Engines.Token
{
    public class BonusTokenSpawnEngine : IQueryingEntitiesEngine, IStep
    {
        private ITokenFactory _tokenFactory;
        private IEntityFunctions _entityFunctions;

        public BonusTokenSpawnEngine(ITokenFactory tokenFactory, IEntityFunctions entityFunctions)
        {
            _tokenFactory = tokenFactory;
            _entityFunctions = entityFunctions;
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
        }

        public void Step(EGID __)
        {
            var turnDatas = entitiesDB.QueryEntities<TurnResultData>(ECSGroups.TurnInProgress, out var turnsCount);
            if (turnsCount == 0)
                return;

            var turn = turnDatas[0];

            if (turn.TokenCount < 4)
                return;

            var tokenTypes = entitiesDB.QueryEntities<TokenTypeData>(ECSGroups.Player, out var count);
            var lastHover = entitiesDB.QueryEntities<TokenHoverData>(ECSGroups.Player, out var hoverCount);

            var id = tokenTypes[0].type;
            var fromGroupId = (int) ECSGroups.CachedBonusTokens + id;

            if (entitiesDB.HasAny<TokenEntityViewStruct>(fromGroupId))
            {
                ReuseEnemy(fromGroupId, lastHover[0].LastHoverPosition);
            }
            else
                _tokenFactory.Build(tokenTypes[0].type, lastHover[0].LastHoverPosition);
        }

        private void ReuseEnemy(int fromGroupId, Vector3 position)
        {
            int count;
            var tokens = entitiesDB.QueryEntities<TokenEntityViewStruct>(fromGroupId, out count);

            if (count > 0)
            {
                tokens[0].PositionCompoenent.position = position;
                tokens[0].EnablerComponent.Enabled = true;

                tokens[0].HilightComponent.Selected = false;
                tokens[0].HilightComponent.Hovered = false;
                tokens[0].HilightComponent.Disappear = false;

                tokens[0].PhysicsComponent.Enabled = true;

                _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(tokens[0].ID, ECSGroups.VisableTokens);
            }
        }
    }
}
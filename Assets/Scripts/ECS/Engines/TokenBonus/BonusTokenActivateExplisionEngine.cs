using System.Collections;
using System.Collections.Generic;
using Data.Game;
using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using Svelto.ECS;
using Svelto.Tasks;
using UnityEngine;

namespace ECS.Engines.Token
{
    public class BonusTokenActivateExplisionEngine : IQueryingEntitiesEngine
    {
        ITaskRoutine _taskRoutine;
        private IEntityFunctions _entityFunctions;

        private int[] _connectedArray;
        private IRayCaster _rayCaster;
        private GameSettings _settings; 
            
        private HashSet<EGID> uniqueOverEgids;

        public BonusTokenActivateExplisionEngine(IEntityFunctions entityFunctions, IRayCaster rayCaster , GameSettings settings)
        {
            _settings = settings;
            _entityFunctions = entityFunctions;
            _taskRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(Run());

            uniqueOverEgids = new HashSet<EGID>();
            _rayCaster = rayCaster;
            _connectedArray = new int[30];
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
            _taskRoutine.Start();
        }

        IEnumerator Run()
        {
            while (true)
            {
                var tokens =
                    entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.ReadyToActivateExplosionTokens,
                        out var count);

                if (count > 0)
                {
                    uniqueOverEgids.Clear();

                    for (int i = 0; i < count; i++)
                    {
                        var hits = _rayCaster.GetHits(tokens[i].PositionCompoenent.position, _settings.ExplosionRadius,
                            _connectedArray);

                        for (int j = 0; j < hits; j++)
                        {
                            var egid = new EGID(_connectedArray[j], ECSGroups.VisableTokens);
                            if (entitiesDB.Exists<TokenEntityViewStruct>(egid))
                            {
                                if (uniqueOverEgids.Contains(egid) == false)
                                {
                                    uniqueOverEgids.Add(egid);

//                                    var dest = entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(egid, out var k);
//                                    Debug.DrawLine(tokens[i].PositionCompoenent.position,dest[k].PositionCompoenent.position);
                                }
                            }
                        }

                        _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(tokens[i].ID, ECSGroups.ActivatedTokens);
                        
//                        Debug.Break();
                    }

                    foreach (EGID egid in uniqueOverEgids)
                    {
                        _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(egid,
                            ECSGroups.PrepareToActivate);
                    }
                }

                yield return null;
            }
        }
    }
}
using ECS.Descriptors.Token;
using ECS.EntityViews.Token;
using Svelto.ECS;

namespace DefaultNamespace.Engines.Input
{
    public class HoverTokenRemoveEngine : IQueryingEntitiesEngine, IStep
    {
        private IEntityFunctions _entityFunctions;

        public HoverTokenRemoveEngine(IEntityFunctions entityFunctions)
        {
            _entityFunctions = entityFunctions;
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
        }

        public void Step(EGID id)
        {
            RemoveIfExistsLastHover();
        }


        private void RemoveIfExistsLastHover()
        {
            if (entitiesDB.HasAny<TokenHoverData>(ECSGroups.Player) == false)
                return;

            var hoverData = entitiesDB.QueryEntities<TokenHoverData>(ECSGroups.Player, out var count);
            for (int i = 0; i < count; i++)
            {
                _entityFunctions.RemoveEntity<TokenHoverDescriptor>(hoverData[i].ID);
            }
        }
    }
}
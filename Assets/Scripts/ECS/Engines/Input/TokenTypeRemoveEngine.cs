using ECS.Descriptors.Token;
using ECS.EntityViews.Token;
using Svelto.ECS;

namespace DefaultNamespace.Engines.Input
{
    public class TokenTypeRemoveEngine:  IQueryingEntitiesEngine, IStep
    {
        public IEntitiesDB entitiesDB { get; set; }
        private readonly IEntityFunctions _entityFunctions;

        public TokenTypeRemoveEngine(IEntityFunctions entityFunctions)
        {
            _entityFunctions = entityFunctions;
        }

        public void Ready()
        {
        }

        public void Step(EGID id)
        {
            //if we previosly stored type
            if (entitiesDB.HasAny<TokenTypeData>(ECSGroups.Player) == false)
            {
                return;
            }

            //remove stored type
            var hoverdOver = entitiesDB.QueryEntities<TokenTypeData>(ECSGroups.Player, out _);
            _entityFunctions.RemoveEntity<TokenTypeEntityDescriptor>(hoverdOver[0].ID);
        }
    }
}
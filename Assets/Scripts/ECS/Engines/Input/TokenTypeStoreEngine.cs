using System;
using System.Collections;
using Data.Game;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.EntityViews.Player;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using UnityEditor;
using UnityEngine;
using Utility;

namespace DefaultNamespace.Engines.Input
{
    public class TokenTypeStoreEngine : IQueryingEntitiesEngine, IStep
    {
        public IEntitiesDB entitiesDB { get; set; }

        private readonly IRayCaster _rayCaster;
        private readonly IEntityFactory _entityFactory;
        private readonly ICounter _counter;
        private GameSettings _gameSettings;
        
        public TokenTypeStoreEngine(IRayCaster rayCaster, IEntityFactory entityFactory, ICounter counter,GameSettings gs)
        {
            _gameSettings = gs;
            _rayCaster = rayCaster;
            _entityFactory = entityFactory;
            _counter = counter;
        }

        public void Ready()
        {
        }

        public void Step(EGID __)
        {
            var playerInputs = entitiesDB.QueryEntities<PlayerInputDataStruct>(ECSGroups.Player, out var targetCount);
            ///raycast for Token
            if (_rayCaster.CheckHit(playerInputs[0].MouseRay, _gameSettings.DistanceBetweenCameraAndTokens,
                GAME_LAYERS.TOKEN_LAYER,
                GAME_LAYERS.TOKEN_MASK, out _,
                out var id))
            {
                //Compose and check if token is active
                var egid = new EGID(id, ECSGroups.VisableTokens);
                if (entitiesDB.Exists<TokenTypeData>(egid))
                {
                    //get hover target
                    var hoverdOver = entitiesDB.QueryEntitiesAndIndex<TokenTypeData>(egid, out uint index);

                    //create entity and store its type
                    EGID typeEgid = new EGID(_counter.NextIndex, ECSGroups.Player);
                    var initializer = _entityFactory.BuildEntity<TokenTypeEntityDescriptor>(typeEgid, null);
                    initializer.Init(new TokenTypeData() {type = hoverdOver[index].type});
                }
            }
        }
    }
}
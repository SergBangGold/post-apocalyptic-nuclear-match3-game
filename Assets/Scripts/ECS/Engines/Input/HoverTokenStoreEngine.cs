using System;
using Data.Game;
using DefaultNamespace.EntityViews.Token;
using ECS.Descriptors.Token;
using ECS.EntityViews.Player;
using ECS.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using UnityEngine;
using Utility;

namespace DefaultNamespace.Engines.Input
{
    public class HoverTokenStoreEngine : IQueryingEntitiesEngine, IStep
    {
        private ITaskRoutine _taskRoutine;
        private IRayCaster _rayCaster;

        private IEntityFunctions _entityFunctions;
        private IEntityFactory _entityFactory;
        private ICounter _counter;
        private GameSettings _gameSettings;


        public HoverTokenStoreEngine(IRayCaster rayCaster, IEntityFunctions entityFunctions,
            IEntityFactory entityFactory, ICounter counter, GameSettings gameSettings)
        {
            _gameSettings = gameSettings;
            _rayCaster = rayCaster;
            _entityFunctions = entityFunctions;
            _entityFactory = entityFactory;
            _counter = counter;
        }

        public IEntitiesDB entitiesDB { get; set; }

        public void Ready()
        {
        }

        public void Step( EGID __)
        {
            CheckForNewHovers();
        }

        private void CheckForNewHovers()
        {
            var playerInputs = entitiesDB.QueryEntities<PlayerInputDataStruct>(ECSGroups.Player, out _);
            //raycast gets token egid
            if (_rayCaster.CheckHit(playerInputs[0].MouseRay, _gameSettings.DistanceBetweenCameraAndTokens,
                GAME_LAYERS.TOKEN_LAYER,
                GAME_LAYERS.TOKEN_MASK, out _,
                out var id))
            {
                var raycastedEgid = new EGID(id, ECSGroups.SelectedTokens);
                if (entitiesDB.Exists<TokenTypeData>(raycastedEgid))
                {
                    var hoverViews =
                        entitiesDB.QueryEntitiesAndIndex<TokenEntityViewStruct>(raycastedEgid, out var index);
                    
                    
                    //if not exists hover, create it and initilize
                    var hoverOvers = entitiesDB.QueryEntities<TokenHoverData>(ECSGroups.Player, out var hoverCount);
                    if (hoverCount == 0)
                    {
                        CreateNewHoverOverData(raycastedEgid, hoverViews[index].PositionCompoenent.position);
                        return;
                    }

                    //if exists query existing
                    //compare with raycasted
                    //if not the same , recreate
                    if (hoverOvers[0].storeEGID != raycastedEgid)
                    {
                        //remove and recreate
                        RemoveIfExistsLastHover();
                        CreateNewHoverOverData(raycastedEgid,hoverViews[index].PositionCompoenent.position);
                    }
                }
            }
        }

        private void CreateNewHoverOverData(EGID raycastedEgid, Vector3 position)
        {
            var newHoverEgid = new EGID(_counter.NextIndex, ECSGroups.Player);
            var initilizer = _entityFactory.BuildEntity<TokenHoverDescriptor>(newHoverEgid, null);
            initilizer.Init(new TokenHoverData() {storeEGID = raycastedEgid, LastHoverPosition = position});
        }

        private void RemoveIfExistsLastHover()
        {
            if (entitiesDB.HasAny<TokenHoverData>(ECSGroups.Player) == false)
                return;

            var hoverData = entitiesDB.QueryEntities<TokenHoverData>(ECSGroups.Player, out var count);
            for (int i = 0; i < count; i++)
            {
                _entityFunctions.RemoveEntity<TokenHoverDescriptor>(hoverData[i].ID);
            }
        }
    }
}
using System.Collections;
using ECS.EntityViews.Player;
using Svelto.ECS;
using Svelto.Tasks;

namespace DefaultNamespace.Engines.Input
{
    public class PlayerInputEngine : SingleEntityEngine<PlayerInputDataStruct>, IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }
        readonly ITaskRoutine _taskRoutine;

        public void Ready()
        {
        }

        public PlayerInputEngine()
        {
            _taskRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(ReadInput());
        }

        IEnumerator ReadInput()
        {
            while (entitiesDB.HasAny<PlayerInputDataStruct>(ECSGroups.Player) == false)
            {
                yield return null;
            }

            while (true)
            {
                int targetCount;
                var playerInputs = entitiesDB.QueryEntities<PlayerInputDataStruct>(ECSGroups.Player, out targetCount);

                playerInputs[0].MouseRay = UnityEngine.Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
                playerInputs[0].MouseDown = UnityEngine.Input.GetMouseButtonDown(0);
                playerInputs[0].IsMouseDown = UnityEngine.Input.GetMouseButton(0);
                playerInputs[0].MouseUp = UnityEngine.Input.GetMouseButtonUp(0);
                yield return null;
            }
        }

        protected override void Add(ref PlayerInputDataStruct entityView)
        {
            _taskRoutine.Start();
        }

        protected override void Remove(ref PlayerInputDataStruct entityView)
        {
            _taskRoutine.Stop();
        }
    }
}
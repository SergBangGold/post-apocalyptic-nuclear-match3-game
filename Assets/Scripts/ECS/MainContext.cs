using Data.Game;
using DefaultNamespace.Descriptors.Player;
using DefaultNamespace.Descriptors.Turn;
using DefaultNamespace.Descriptors.UI;
using DefaultNamespace.Engines;
using DefaultNamespace.Engines.Input;
using DefaultNamespace.Engines.Turn;
using DefaultNamespace.EntityViews.Token;
using DefaultNamespace.Factories.TokenConnection;
using ECS.Descriptors.Token;
using ECS.Engines.Token;
using ECS.Engines.TokenConnection;
using ECS.Engines.UI;
using ECS.Factories.Token;
using ECS.Implementors;
using Svelto.Context;
using Svelto.ECS;
using Svelto.ECS.Schedulers.Unity;
using Svelto.Tasks;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Utility;

namespace DefaultNamespace
{
    public class Main : ICompositionRoot
    {
        EnginesRoot _enginesRoot;
        IEntityFactory _entityFactory;
        UnityEntitySubmissionScheduler _unityEntitySubmissionScheduler;

        private GameData _gameData;
        private GameSettings _gameSettings;
        private CharacterDb _characterDb;

        //
        private ITokenConnectionLineFactory lineFactory;
        private GameObjectFactory _gameObjectFactory;
        private ICounter counter;

        public Main()
        {
            InitData();
            SetupEngines();
            InitEntities();
        }

        private void InitData()
        {
            _gameData = DataKeeper.LoadFromStreamingAssetsPath<GameData>(GameData.FileName);
            _gameSettings = DataKeeper.LoadFromStreamingAssetsPath<GameSettings>(GameSettings.FileName);
            _characterDb =  DataKeeper.LoadFromStreamingAssetsPath<CharacterDb>(CharacterDb.FileName);
        }

        private void SetupEngines()
        {
            _unityEntitySubmissionScheduler = new UnityEntitySubmissionScheduler();
            _enginesRoot = new EnginesRoot(_unityEntitySubmissionScheduler);
            _entityFactory = _enginesRoot.GenerateEntityFactory();
            var entityFunctions = _enginesRoot.GenerateEntityFunctions();


            //Factories
            _gameObjectFactory = new GameObjectFactory();
            ITokenFactory tokenFactory = new TokenFactory(_entityFactory, _gameObjectFactory, _gameData);
            lineFactory = new TokenConnectionLineFactory(_entityFactory, _gameObjectFactory);
            ITokenFactory bonusTokenFactory = new BonusTokenFactory(_entityFactory, _gameObjectFactory, _gameData);


            //Services
            IRayCaster raycaster = new RayCaster();
            counter = new SimpleCounter();

            //Sequencers
            TurnSequencer turnSequencer = new TurnSequencer();

            //Player
            PlayerInputEngine playerInputEngine = new PlayerInputEngine();
            TokenTypeStoreEngine tokenTypeStoreEngine =
                new TokenTypeStoreEngine(raycaster, _entityFactory, counter, _gameSettings);
            TokenTypeRemoveEngine tokenTypeRemoveEngine = new TokenTypeRemoveEngine(entityFunctions);
            HoverTokenStoreEngine hoverTokenStoreEngine =
                new HoverTokenStoreEngine(raycaster, entityFunctions, _entityFactory, counter, _gameSettings);
            HoverTokenRemoveEngine hoverTokenRemoveEngine = new HoverTokenRemoveEngine(entityFunctions);

            //Tokens
            TokenSpawnerEngine tokenSpawnerEngine = new TokenSpawnerEngine(tokenFactory, entityFunctions);
            TokenSelectEngine tokenSelectEngine = new TokenSelectEngine(entityFunctions, raycaster, _gameSettings);
            TokenDespawnEngine tokenDespawnEngine = new TokenDespawnEngine(entityFunctions);
            TokenUnSelectEngine tokenUnSelectEngine = new TokenUnSelectEngine(entityFunctions, turnSequencer);
            TokenSplitActivateEngine tokenSplitActivateEngine =
                new TokenSplitActivateEngine(turnSequencer, entityFunctions);
            TokenHoverOverAnimationEngine tokenHoverOverAnimationEngine = new TokenHoverOverAnimationEngine();
            TokenEnablePhysicsEngine tokenEnablePhysicsEngine = new TokenEnablePhysicsEngine();
            TokenDisablePhysicsEngine tokenDisablePhysicsEngine = new TokenDisablePhysicsEngine();
            TokenDisppearEngine tokenDisppearEngine = new TokenDisppearEngine(turnSequencer, entityFunctions);
            TokenActivateEngine tokenActivateEngine = new TokenActivateEngine(entityFunctions, _entityFactory, counter);


            //BonusTokens
            BonusTokenSpawnEngine bonusTokenSpawnEngine = new BonusTokenSpawnEngine(bonusTokenFactory, entityFunctions);
            BonusTokenActivateExplisionEngine bonusTokenActivateExplisionEngine =
                new BonusTokenActivateExplisionEngine(entityFunctions, raycaster, _gameSettings);

            //TokenConnections
            TokenConnectionEngine tokenConnectionEngine =
                new TokenConnectionEngine(_entityFactory, counter, entityFunctions);

            //UI
            UiDamageUpdaterEngine uiDamageUpdaterEngine = new UiDamageUpdaterEngine();

            //turn
            TurnResultCalculationEngine turnResultCalculationEngine = new TurnResultCalculationEngine();
            TurnBeginHandlerEngine turnBeginHandlerEngine =
                new TurnBeginHandlerEngine(turnSequencer, entityFunctions, counter, _entityFactory);
            TurnInProgressHandlerEngine turnInProgressHandlerEngine =
                new TurnInProgressHandlerEngine(turnSequencer, entityFunctions);
            TurnEndingHandlerEngine turnEndingHandlerEngine =
                new TurnEndingHandlerEngine(turnSequencer, entityFunctions);
            TurnResetHandlerEngine turnResetHandlerEngine = new TurnResetHandlerEngine(entityFunctions);
            TurnTokensActivateUnselectDesigion tokensActivateUnselectDesigion =
                new TurnTokensActivateUnselectDesigion(entityFunctions);
            TurnActivationDoneTrackerEngine turnActivationDoneTrackerEngine =
                new TurnActivationDoneTrackerEngine(turnSequencer, entityFunctions);

            //Game
            GameEngine gameEngine = new GameEngine(_characterDb);

            //input
            _enginesRoot.AddEngine(playerInputEngine);
            _enginesRoot.AddEngine(hoverTokenStoreEngine);
            _enginesRoot.AddEngine(hoverTokenRemoveEngine);
            _enginesRoot.AddEngine(tokenTypeStoreEngine);
            _enginesRoot.AddEngine(tokenTypeRemoveEngine);

            //tokens
            _enginesRoot.AddEngine(tokenSpawnerEngine);
            _enginesRoot.AddEngine(tokenSelectEngine);
            _enginesRoot.AddEngine(tokenUnSelectEngine);
            _enginesRoot.AddEngine(tokenSplitActivateEngine);
            _enginesRoot.AddEngine(tokenDisppearEngine);
            _enginesRoot.AddEngine(tokenDespawnEngine);
            _enginesRoot.AddEngine(tokenHoverOverAnimationEngine);
            _enginesRoot.AddEngine(tokenEnablePhysicsEngine);
            _enginesRoot.AddEngine(tokenDisablePhysicsEngine);
            _enginesRoot.AddEngine(tokenActivateEngine);

            //BonusTokens
            _enginesRoot.AddEngine(bonusTokenSpawnEngine);
            _enginesRoot.AddEngine(bonusTokenActivateExplisionEngine);

            //Connections
            _enginesRoot.AddEngine(tokenConnectionEngine);

            //UI
            _enginesRoot.AddEngine(uiDamageUpdaterEngine);

            //turn
            _enginesRoot.AddEngine(turnResultCalculationEngine);
            _enginesRoot.AddEngine(turnBeginHandlerEngine);
            _enginesRoot.AddEngine(turnInProgressHandlerEngine);
            _enginesRoot.AddEngine(turnEndingHandlerEngine);
            _enginesRoot.AddEngine(turnResetHandlerEngine);
            _enginesRoot.AddEngine(tokensActivateUnselectDesigion);
            _enginesRoot.AddEngine(turnActivationDoneTrackerEngine);

            //Game
            _enginesRoot.AddEngine(gameEngine);


            //Setup Sequencers
            turnSequencer.SetSequence(tokenTypeStoreEngine, tokenTypeRemoveEngine,
                hoverTokenStoreEngine, hoverTokenRemoveEngine,
//                turnResultCalculationEngine,
                bonusTokenSpawnEngine,
                tokensActivateUnselectDesigion,
                tokenUnSelectEngine,
                tokenSplitActivateEngine, tokenDisppearEngine, tokenSelectEngine,
                tokenEnablePhysicsEngine, tokenDisablePhysicsEngine,
                tokenDespawnEngine,
                turnBeginHandlerEngine,
                turnInProgressHandlerEngine,
                turnEndingHandlerEngine,
                turnResetHandlerEngine,
                turnActivationDoneTrackerEngine
            );
        }

        private void InitEntities()
        {
            //Player
            var initializer = _entityFactory.BuildEntity<PlayerDescriptor>(new EGID(0, ECSGroups.Player), null);

            //Line
            lineFactory.Build(_gameData.linePrefab);

            //UI
            var combatUIGO = _gameObjectFactory.Build(_gameData.UICombat);
            var combatUIimplementors = combatUIGO.GetComponentsInChildren<IImplementor>();
            var combatUIEgid = new EGID(combatUIGO.GetInstanceID(), ECSGroups.CombatUI);
            _entityFactory.BuildEntity<CombatUIDescriptor>(combatUIEgid, combatUIimplementors);
            combatUIGO.SetActive(false);
            
            var combatSetupUIGO = _gameObjectFactory.Build(_gameData.UICombatSetup);
            var combatSetupIimplementors = combatSetupUIGO.GetComponentsInChildren<IImplementor>();
            var  combatSetupUIEgid = new EGID(combatSetupUIGO.GetInstanceID(), ECSGroups.CombatSetupUI);
            _entityFactory.BuildEntity<CombatSetupUIDescritor>(combatSetupUIEgid, combatSetupIimplementors);
            combatSetupUIGO.SetActive(false);

            //turn
            var turnEgid = new EGID(counter.NextIndex, ECSGroups.TurnBegin);
            _entityFactory.BuildEntity<TurnDescriptor>(turnEgid, null);
        }

        public void OnContextInitialized()
        {
        }

        public void OnContextDestroyed()
        {
            //final clean up
            _enginesRoot.Dispose();

            //Tasks can run across level loading, so if you don't want
            //that, the runners must be stopped explicitly.
            //careful because if you don't do it and 
            //unintentionally leave tasks running, you will cause leaks
            TaskRunner.StopAndCleanupAllDefaultSchedulers();
        }

        public void OnContextCreated<T>(T contextHolder)
        {
            BuildEntitiesFromScene(contextHolder as UnityContext);
        }

        void BuildEntitiesFromScene(UnityContext contextHolder)
        {
            //An EntityDescriptorHolder is a special Svelto.ECS class created to exploit
            //GameObjects to dynamically retrieve the Entity information attached to it.
            //Basically a GameObject can be used to hold all the information needed to create
            //an Entity and later queries to build the entitity itself.
            //This allows to trigger a sort of polymorphic code that can be re-used to 
            //create several type of entities.

            IEntityDescriptorHolder[] entities = contextHolder.GetComponentsInChildren<IEntityDescriptorHolder>();

            //However this common pattern in Svelto.ECS application exists to automatically
            //create entities from gameobjects already presented in the scene.
            //I still suggest to avoid this method though and create entities always
            //manually and explicitly. Basically EntityDescriptorHolder should be avoided
            //whenever not strictly necessary.

//            for (int i = 0; i < entities.Length; i++)
//            {
//                var entityDescriptorHolder = entities[i];
//                var entityViewsToBuild     = entityDescriptorHolder.GetDescriptor();
//                _entityFactory.BuildEntity
//                (new EGID(((MonoBehaviour) entityDescriptorHolder).gameObject.GetInstanceID(), ECSGroups.ExtraStuff),
//                    entityViewsToBuild,
//                    (entityDescriptorHolder as MonoBehaviour).GetComponentsInChildren<IImplementor>());
//            }
        }
    }

    public class MainContext : UnityContext<Main>
    {
    }
}
using DefaultNamespace.EntityViews.BonusToken;
using DefaultNamespace.EntityViews.Token;
using ECS.EntityViews.Token;
using Svelto.ECS;

namespace ECS.Descriptors.Token
{
    public class BonusExposionTokenDescriptor : ExtendibleEntityDescriptor<BaseTokenDescriptor>
    {
        public BonusExposionTokenDescriptor() : base(new IEntityBuilder[]
        {
            new EntityBuilder<TokenEntityViewStruct>(),
            new EntityBuilder<BonusTokenExplosionData>(),
        })
        {
        }
    }
}
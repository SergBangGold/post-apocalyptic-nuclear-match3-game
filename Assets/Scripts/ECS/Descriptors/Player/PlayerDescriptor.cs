using ECS.EntityViews.Player;
using Svelto.ECS;

namespace DefaultNamespace.Descriptors.Player
{
    public struct PlayerDescriptor : IEntityDescriptor
    {
        public IEntityBuilder[] entitiesToBuild => new IEntityBuilder[]{
            new EntityBuilder<PlayerInputDataStruct>()
        };
}
}
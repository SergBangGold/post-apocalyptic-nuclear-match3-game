using DefaultNamespace.EntityViews.Token;
using ECS.EntityViews.Token;
using Svelto.ECS;

namespace ECS.Descriptors.Token
{
    public class TokenDescriptor : ExtendibleEntityDescriptor<BaseTokenDescriptor>
    {
        public TokenDescriptor() : base(new IEntityBuilder[]
        {
            new EntityBuilder<TokenEntityViewStruct>(),
            new EntityBuilder<TokenDamageData>(),
        })
        {
        }
    }
}
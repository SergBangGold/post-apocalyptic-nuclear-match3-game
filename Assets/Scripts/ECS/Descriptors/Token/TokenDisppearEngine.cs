using System.Collections;
using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using Svelto.ECS;
using Svelto.Tasks;
using Svelto.Tasks.Enumerators;

namespace ECS.Descriptors.Token
{
    public class TokenDisppearEngine : IQueryingEntitiesEngine
    {
        public IEntitiesDB entitiesDB { get; set; }

        private TurnSequencer _turnSequencer;

        private WaitForSecondsEnumerator _waitForSecondsEnumerator;

        private IEntityFunctions _entityFunctions;

        private ITaskRoutine _dissapearRoutine;
        private ITaskRoutine _watchRoutine;

        public TokenDisppearEngine(TurnSequencer turnSequencer, IEntityFunctions entityFunctions)
        {
            _turnSequencer = turnSequencer;
            _entityFunctions = entityFunctions;

            _waitForSecondsEnumerator = new WaitForSecondsEnumerator(0.8f);
            _dissapearRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumeratorProvider(Dissappear);
            _watchRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumeratorProvider(Watch);
        }

        public void Ready()
        {
            _watchRoutine.Start();
        }


        IEnumerator Watch()
        {
            while (true)
            {

                if (entitiesDB.HasAny<TokenEntityViewStruct>(ECSGroups.ActivatedTokens))
                {
                    _dissapearRoutine.Start();
                }
                
                yield return null;
            }
        }

        IEnumerator Dissappear()
        {
            _watchRoutine.Stop();
            var selected = entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.ActivatedTokens, out var count);

            for (int i = 0; i < count; i++)
            {
                //play animation
                selected[i].HilightComponent.Disappear = true;
                _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(selected[i].ID,ECSGroups.DisapearingTokens);
            }

            //after complete continue
            yield return _waitForSecondsEnumerator;

            var dissappeard = entitiesDB.QueryEntities<TokenEntityViewStruct>(ECSGroups.DisapearingTokens, out var dissappeardCount);
            
            for (int i = 0; i < dissappeardCount; i++)
            {
                _entityFunctions.SwapEntityGroup<BaseTokenDescriptor>(dissappeard[i].ID, ECSGroups.DisapearedTokens);
            }

            _watchRoutine.Start();
        }
    }
}
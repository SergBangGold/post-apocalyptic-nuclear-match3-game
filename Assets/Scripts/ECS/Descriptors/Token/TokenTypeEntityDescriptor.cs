using ECS.EntityViews.Token;
using Svelto.ECS;

namespace ECS.Descriptors.Token
{
    public class TokenTypeEntityDescriptor : IEntityDescriptor
    {
        public IEntityBuilder[] entitiesToBuild => new IEntityBuilder[] {new EntityBuilder<TokenTypeData>()};
    }
}
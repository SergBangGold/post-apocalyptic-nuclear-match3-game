using ECS.EntityViews.TokenConnection;
using Svelto.ECS;

namespace ECS.Descriptors.TokenConnection
{
    public class TokenConnectionEntityViewDescriptor : IEntityDescriptor
    {
        public IEntityBuilder[] entitiesToBuild => new[] {new EntityBuilder<TokenConnectionEntityViewStruct>()};
    }
}
using DefaultNamespace.EntityViews.TokenConnection;
using Svelto.ECS;

namespace ECS.Descriptors.TokenConnection
{
    public class TokenConnectionPointDescriptor : GenericEntityDescriptor<TokenConnectionPointData>
    {
    }
}
using Data.Game;
using DefaultNamespace;
using DefaultNamespace.EntityViews.Token;
using DefaultNamespace.Implementors;
using ECS.Descriptors.Token;
using ECS.EntityViews.Token;
using ECS.Implementors;
using Svelto.ECS;
using Svelto.Factories;
using UnityEngine;

namespace ECS.Factories.Token
{
    public class TokenFactory : ITokenFactory
    {
        private IEntityFactory _entityFactory;
        private IGameObjectFactory _gameObjectFactory;
        private GameData _gameData;

        private GameObject _root;
        
        public TokenFactory(IEntityFactory entityFactory, IGameObjectFactory gameObjectFactory, GameData gameData)
        {
            _entityFactory = entityFactory;
            _gameObjectFactory = gameObjectFactory;
            _gameData = gameData;
            
            _root = new GameObject("Tokens");
        }

        public void Build( int id, Vector3 position)
        {
            if (id > 3)
            {
                Debug.Log("Id more then max");
                return;
            }

            GameObject go = _gameObjectFactory.Build(_gameData.tokenPrefabs[id]);
            go.transform.position = position;
            go.transform.SetParent(_root.transform);
            
            var implementors = go.GetComponentsInChildren<IImplementor>();

            var initializer =
                _entityFactory.BuildEntity<TokenDescriptor>(go.GetInstanceID(),
                    ECSGroups.VisableTokens , implementors);
            initializer.Init(new TokenTypeData() {type = id});
            initializer.Init(new TokenDamageData(){Value =  1});
        }
    }
}
using UnityEngine;

namespace ECS.Factories.Token
{
    public interface ITokenFactory
    {
        void Build( int id , Vector3 position );
    }
}
using Data.Game;
using DefaultNamespace;
using ECS.Descriptors.Token;
using ECS.EntityViews.Token;
using ECS.Implementors;
using Svelto.ECS;
using Svelto.Factories;
using UnityEngine;

namespace ECS.Factories.Token
{
    public class BonusTokenFactory : ITokenFactory
    {
        private IEntityFactory _entityFactory;
        private IGameObjectFactory _gameObjectFactory;
        private GameData _gameData;

        private GameObject _root;

        public BonusTokenFactory(IEntityFactory entityFactory, IGameObjectFactory gameObjectFactory, GameData gameData)
        {
            _entityFactory = entityFactory;
            _gameObjectFactory = gameObjectFactory;
            _gameData = gameData;
            _root = new GameObject("BoonusTokens");
        }

        public void Build(int id, Vector3 position)
        {
            if (id > 3)
            {
                Debug.Log("Id more then max");
                return;
            }

            GameObject go = _gameObjectFactory.Build(_gameData.tokenBonusPrefabs[id]);
            go.transform.position = position;
            go.transform.SetParent(_root.transform);
            var implementors = go.GetComponentsInChildren<IImplementor>();

            var initializer =
                _entityFactory.BuildEntity<BonusExposionTokenDescriptor>(go.GetInstanceID(),
                    ECSGroups.VisableTokens, implementors);
            initializer.Init(new TokenTypeData() {type = id});
        }
    }
}
using UnityEngine;

namespace DefaultNamespace.Factories.TokenConnection
{
    public interface ITokenConnectionLineFactory
    {
        void Build(GameObject prefab);
    }
}
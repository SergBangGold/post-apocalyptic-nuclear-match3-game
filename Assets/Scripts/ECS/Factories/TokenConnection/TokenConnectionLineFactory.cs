using ECS.Descriptors.Token;
using ECS.Descriptors.TokenConnection;
using ECS.EntityViews.TokenConnection;
using ECS.Implementors;
using Svelto.ECS;
using Svelto.Factories;
using UnityEngine;

namespace DefaultNamespace.Factories.TokenConnection
{
    public class TokenConnectionLineFactory : ITokenConnectionLineFactory
    {
        private IEntityFactory _entityFactory;
        private IGameObjectFactory _gameObjectFactory;

        public TokenConnectionLineFactory(IEntityFactory entityFactory, IGameObjectFactory gameObjectFactory)
        {
            _entityFactory = entityFactory;
            _gameObjectFactory = gameObjectFactory;
        }

        public void Build(GameObject prefab)
        {
            GameObject go = _gameObjectFactory.Build(prefab);
            var implementors = go.GetComponentsInChildren<IImplementor>();

            _entityFactory.BuildEntity<TokenConnectionEntityViewDescriptor>(go.GetInstanceID(),
                ECSGroups.ConnectionLine, implementors);
            go.SetActive(false);
        }
    }
}
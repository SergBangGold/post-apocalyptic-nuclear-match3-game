using Svelto.ECS;

namespace DefaultNamespace
{
    public static class ECSGroups
    {
        public static readonly ExclusiveGroup Player = new ExclusiveGroup();

        public static readonly ExclusiveGroup CachedTokens = new ExclusiveGroup(4);
        public static readonly ExclusiveGroup CachedBonusTokens = new ExclusiveGroup(4);

        public static readonly ExclusiveGroup VisableTokens = new ExclusiveGroup();
        public static readonly ExclusiveGroup SelectedTokens = new ExclusiveGroup();

        public static readonly ExclusiveGroup PrepareToActivate = new ExclusiveGroup();
        public static readonly ExclusiveGroup PrepareToDeActivate = new ExclusiveGroup();


        public static readonly ExclusiveGroup ReadyToActivateTokens = new ExclusiveGroup();
        public static readonly ExclusiveGroup ReadyToActivateExplosionTokens = new ExclusiveGroup();
        public static readonly ExclusiveGroup ActivatedTokens = new ExclusiveGroup();
        public static readonly ExclusiveGroup DisapearingTokens = new ExclusiveGroup();
        public static readonly ExclusiveGroup DisapearedTokens = new ExclusiveGroup();
        public static readonly ExclusiveGroup ReadyToCacheTokens = new ExclusiveGroup(2);

        public static readonly ExclusiveGroup[] DisableGravityTokens = new ExclusiveGroup[]
        {
            VisableTokens, SelectedTokens, PrepareToActivate, PrepareToDeActivate, ReadyToActivateTokens,
            ReadyToActivateExplosionTokens, ActivatedTokens, DisapearingTokens, DisapearedTokens, ReadyToCacheTokens
        };

        public static readonly ExclusiveGroup[] TurnInProgressMarkerGroup = new ExclusiveGroup[]
        {
            SelectedTokens, PrepareToActivate, PrepareToDeActivate, ReadyToActivateTokens,
            ReadyToActivateExplosionTokens, ActivatedTokens, DisapearingTokens, DisapearedTokens, ReadyToCacheTokens
        };

        public static readonly ExclusiveGroup ConnectionLine = new ExclusiveGroup();

        public static readonly ExclusiveGroup TurnBegin = new ExclusiveGroup();
        public static readonly ExclusiveGroup TurnInProgress = new ExclusiveGroup();
        public static readonly ExclusiveGroup TurnEnding = new ExclusiveGroup();
        public static readonly ExclusiveGroup TurnComplete = new ExclusiveGroup();

        public static readonly ExclusiveGroup CombatUI = new ExclusiveGroup();
        public static readonly ExclusiveGroup CombatSetupUI = new ExclusiveGroup();
        
        public static readonly ExclusiveGroup TeamA = new ExclusiveGroup();
        public static readonly ExclusiveGroup TeamB = new ExclusiveGroup();
    }
}
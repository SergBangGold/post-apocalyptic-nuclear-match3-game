using System;
using System.Collections.Generic;
using DefaultNamespace.Engines;
using ECS.Components;
using UnityWeld.Binding;

namespace DefaultNamespace
{
    public interface ICombatSetupUIComponent : IComponent
    {
        void ClearAvaliable();
        void AddAvaliable(List<CharacterDb.CharacterConfig> characters, List<float> characterDbRankings);
        
        event Action <GameEngine.Teams , CharacterDb.CharacterConfig > AddToTeam;
        event Action <GameEngine.Teams , CharacterDb.CharacterConfig > RemoveFromTeam;

        ObservableList<CharacterDb.CharacterConfig> TeamA { get; }
        ObservableList<CharacterDb.CharacterConfig> TeamB { get; }

    }
}
using System;
using ECS.Components;

namespace DefaultNamespace
{
    public interface IStarCombatComponent: IComponent
    {
        event Action OnClick ;
    }
}
using System;

namespace ECS.Components.UI
{
    public interface IBackComponent : IComponent
    {
        event Action OnClick ;
    }
}
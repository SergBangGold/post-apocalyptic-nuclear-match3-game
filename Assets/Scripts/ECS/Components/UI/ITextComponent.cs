namespace ECS.Components.UI
{
    public interface ITextComponent : IComponent
    {
        void UpdateText(string text);
    }
}
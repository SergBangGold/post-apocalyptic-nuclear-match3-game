using System.Collections.Generic;
using UnityEngine;

namespace ECS.Components.TokenConnection
{
    public interface ITokenConnectionHilighter  : IComponent
    {
        void UpdatePoints(List<Vector3> list);
    }
}
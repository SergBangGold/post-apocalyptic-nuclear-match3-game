namespace ECS.Components.Token
{
    public interface IPhisicsComponent : IComponent
    {
        bool Enabled { set; }
    }
}
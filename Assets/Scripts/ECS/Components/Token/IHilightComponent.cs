
using Svelto.ECS;

namespace ECS.Components.Token
{
    public interface IHilightComponent : IComponent
    {
        bool Selected { set; }
        bool Hovered { set; }
        bool Disappear { set; }
    }
}
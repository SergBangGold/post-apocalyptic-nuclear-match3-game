using ECS.Components;
using UnityEngine;
using UnityEngine.Experimental.UIElements.StyleEnums;

namespace DefaultNamespace.Components.CommonComponents
{
    public interface IPositionCompoenent :IComponent
    {
        Vector3 position { get; set; }
    }

    public interface ITransfromComponent : IPositionCompoenent
    {
        Quaternion orientation { get; set; }
    }

    public interface IEnablerComponent : IComponent
    {
        bool Enabled { set; }
    }
    
}
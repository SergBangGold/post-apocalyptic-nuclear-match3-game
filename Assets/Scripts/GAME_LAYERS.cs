using UnityEngine;

namespace DefaultNamespace
{
    public static class GAME_LAYERS
    {
        public static readonly int TOKEN_MASK = LayerMask.GetMask("Default");
        public static readonly int TOKEN_LAYER = LayerMask.NameToLayer("Default");
        public static readonly int IGNORE_LAYER = LayerMask.NameToLayer("Ignore Raycast");
    }
}